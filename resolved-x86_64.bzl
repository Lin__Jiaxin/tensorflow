resolved = [
    {
        "original_rule_class": "local_repository",
        "original_attributes": {
            "name": "bazel_tools",
            "path": "/home/abuild/rpmbuild/BUILD/output_user_root/install/f520b02a0d98143caf8e25678d03643a/embedded_tools"
        },
        "native": "local_repository(name = \"bazel_tools\", path = __embedded_dir__ + \"/\" + \"embedded_tools\")"
    },
    {
        "original_rule_class": "@bazel_tools//tools/build_defs/repo:http.bzl%http_archive",
        "definition_information": "Repository io_bazel_rules_closure instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule http_archive defined at:\n  /home/abuild/rpmbuild/BUILD/output_user_root/55287d6e880b1ebc99135e2e30fc9816/external/bazel_tools/tools/build_defs/repo/http.bzl:336:31: in <toplevel>\n",
        "original_attributes": {
            "name": "io_bazel_rules_closure",
            "urls": [
                "https://storage.googleapis.com/mirror.tensorflow.org/github.com/bazelbuild/rules_closure/archive/308b05b2419edb5c8ee0471b67a40403df940149.tar.gz",
                "https://github.com/bazelbuild/rules_closure/archive/308b05b2419edb5c8ee0471b67a40403df940149.tar.gz"
            ],
            "sha256": "5b00383d08dd71f28503736db0500b6fb4dda47489ff5fc6bed42557c07c6ba9",
            "strip_prefix": "rules_closure-308b05b2419edb5c8ee0471b67a40403df940149"
        },
        "repositories": [
            {
                "rule_class": "@bazel_tools//tools/build_defs/repo:http.bzl%http_archive",
                "attributes": {
                    "url": "",
                    "urls": [
                        "https://storage.googleapis.com/mirror.tensorflow.org/github.com/bazelbuild/rules_closure/archive/308b05b2419edb5c8ee0471b67a40403df940149.tar.gz",
                        "https://github.com/bazelbuild/rules_closure/archive/308b05b2419edb5c8ee0471b67a40403df940149.tar.gz"
                    ],
                    "sha256": "5b00383d08dd71f28503736db0500b6fb4dda47489ff5fc6bed42557c07c6ba9",
                    "netrc": "",
                    "auth_patterns": {},
                    "canonical_id": "",
                    "strip_prefix": "rules_closure-308b05b2419edb5c8ee0471b67a40403df940149",
                    "type": "",
                    "patches": [],
                    "patch_tool": "",
                    "patch_args": [
                        "-p0"
                    ],
                    "patch_cmds": [],
                    "patch_cmds_win": [],
                    "build_file_content": "",
                    "workspace_file_content": "",
                    "name": "io_bazel_rules_closure"
                },
                "output_tree_hash": "b3bf45dbab21f41a3457b69d8263658e6792964275a63410e09e5f67de241456"
            }
        ]
    },
    {
        "original_rule_class": "@bazel_tools//tools/build_defs/repo:http.bzl%http_archive",
        "definition_information": "Repository bazel_toolchains instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule http_archive defined at:\n  /home/abuild/rpmbuild/BUILD/output_user_root/55287d6e880b1ebc99135e2e30fc9816/external/bazel_tools/tools/build_defs/repo/http.bzl:336:31: in <toplevel>\n",
        "original_attributes": {
            "name": "bazel_toolchains",
            "generator_name": "bazel_toolchains",
            "generator_function": "bazel_toolchains_archive",
            "urls": [
                "https://storage.googleapis.com/mirror.tensorflow.org/github.com/bazelbuild/bazel-toolchains/archive/92dd8a7a518a2fb7ba992d47c8b38299fe0be825.tar.gz",
                "https://github.com/bazelbuild/bazel-toolchains/archive/92dd8a7a518a2fb7ba992d47c8b38299fe0be825.tar.gz"
            ],
            "sha256": "88e818f9f03628eef609c8429c210ecf265ffe46c2af095f36c7ef8b1855fef5",
            "strip_prefix": "bazel-toolchains-92dd8a7a518a2fb7ba992d47c8b38299fe0be825"
        },
        "repositories": [
            {
                "rule_class": "@bazel_tools//tools/build_defs/repo:http.bzl%http_archive",
                "attributes": {
                    "url": "",
                    "urls": [
                        "https://storage.googleapis.com/mirror.tensorflow.org/github.com/bazelbuild/bazel-toolchains/archive/92dd8a7a518a2fb7ba992d47c8b38299fe0be825.tar.gz",
                        "https://github.com/bazelbuild/bazel-toolchains/archive/92dd8a7a518a2fb7ba992d47c8b38299fe0be825.tar.gz"
                    ],
                    "sha256": "88e818f9f03628eef609c8429c210ecf265ffe46c2af095f36c7ef8b1855fef5",
                    "netrc": "",
                    "auth_patterns": {},
                    "canonical_id": "",
                    "strip_prefix": "bazel-toolchains-92dd8a7a518a2fb7ba992d47c8b38299fe0be825",
                    "type": "",
                    "patches": [],
                    "patch_tool": "",
                    "patch_args": [
                        "-p0"
                    ],
                    "patch_cmds": [],
                    "patch_cmds_win": [],
                    "build_file_content": "",
                    "workspace_file_content": "",
                    "name": "bazel_toolchains"
                },
                "output_tree_hash": "d7783930fd4e7a449dd6f0063a658c6b27b836d78ccea7bae353adda9441c533"
            }
        ]
    },
    {
        "original_rule_class": "@bazel_tools//tools/build_defs/repo:git.bzl%git_repository",
        "definition_information": "Repository io_bazel_rules_docker instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule git_repository defined at:\n  /home/abuild/rpmbuild/BUILD/output_user_root/55287d6e880b1ebc99135e2e30fc9816/external/bazel_tools/tools/build_defs/repo/git.bzl:195:33: in <toplevel>\n",
        "original_attributes": {
            "name": "io_bazel_rules_docker",
            "generator_name": "io_bazel_rules_docker",
            "generator_function": "repositories",
            "remote": "https://github.com/bazelbuild/rules_docker.git",
            "commit": "251f6a68b439744094faff800cd029798edf9faa"
        },
        "repositories": [
            {
                "rule_class": "@bazel_tools//tools/build_defs/repo:git.bzl%git_repository",
                "attributes": {
                    "remote": "https://github.com/bazelbuild/rules_docker.git",
                    "commit": "251f6a68b439744094faff800cd029798edf9faa",
                    "shallow_since": "1556410077 -0400",
                    "init_submodules": False,
                    "verbose": False,
                    "strip_prefix": "",
                    "patches": [],
                    "patch_tool": "",
                    "patch_args": [
                        "-p0"
                    ],
                    "patch_cmds": [],
                    "patch_cmds_win": [],
                    "name": "io_bazel_rules_docker"
                },
                "output_tree_hash": "2c3643bb435d7be52fb6da6098cc9caa4315c44380e77f68bf195884ae84c9d7"
            }
        ]
    },
    {
        "original_rule_class": "//third_party:repo.bzl%tf_http_archive",
        "definition_information": "Repository bazel_skylib instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule tf_http_archive defined at:\n  /home/abuild/rpmbuild/BUILD/tensorflow-2.3.1/third_party/repo.bzl:134:34: in <toplevel>\n",
        "original_attributes": {
            "name": "bazel_skylib",
            "generator_name": "bazel_skylib",
            "generator_function": "tf_repositories",
            "sha256": "1dde365491125a3db70731e25658dfdd3bc5dbdfd11b840b3e987ecf043c7ca0",
            "urls": [
                "https://storage.googleapis.com/mirror.tensorflow.org/github.com/bazelbuild/bazel-skylib/releases/download/0.9.0/bazel_skylib-0.9.0.tar.gz",
                "https://github.com/bazelbuild/bazel-skylib/releases/download/0.9.0/bazel_skylib-0.9.0.tar.gz"
            ]
        },
        "repositories": [
            {
                "rule_class": "//third_party:repo.bzl%tf_http_archive",
                "attributes": {
                    "name": "bazel_skylib",
                    "generator_name": "bazel_skylib",
                    "generator_function": "tf_repositories",
                    "sha256": "1dde365491125a3db70731e25658dfdd3bc5dbdfd11b840b3e987ecf043c7ca0",
                    "urls": [
                        "https://storage.googleapis.com/mirror.tensorflow.org/github.com/bazelbuild/bazel-skylib/releases/download/0.9.0/bazel_skylib-0.9.0.tar.gz",
                        "https://github.com/bazelbuild/bazel-skylib/releases/download/0.9.0/bazel_skylib-0.9.0.tar.gz"
                    ]
                },
                "output_tree_hash": "e0aa02f9484ef42ad8a0f9551637be15433b45b75a3256bb2d03a4986b20a90d"
            }
        ]
    },
    {
        "original_rule_class": "//third_party:repo.bzl%tf_http_archive",
        "definition_information": "Repository build_bazel_rules_swift instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule tf_http_archive defined at:\n  /home/abuild/rpmbuild/BUILD/tensorflow-2.3.1/third_party/repo.bzl:134:34: in <toplevel>\n",
        "original_attributes": {
            "name": "build_bazel_rules_swift",
            "generator_name": "build_bazel_rules_swift",
            "generator_function": "tf_repositories",
            "sha256": "d0833bc6dad817a367936a5f902a0c11318160b5e80a20ece35fb85a5675c886",
            "urls": [
                "https://storage.googleapis.com/mirror.tensorflow.org/github.com/bazelbuild/rules_swift/archive/3eeeb53cebda55b349d64c9fc144e18c5f7c0eb8.tar.gz",
                "https://github.com/bazelbuild/rules_swift/archive/3eeeb53cebda55b349d64c9fc144e18c5f7c0eb8.tar.gz"
            ],
            "strip_prefix": "rules_swift-3eeeb53cebda55b349d64c9fc144e18c5f7c0eb8"
        },
        "repositories": [
            {
                "rule_class": "//third_party:repo.bzl%tf_http_archive",
                "attributes": {
                    "name": "build_bazel_rules_swift",
                    "generator_name": "build_bazel_rules_swift",
                    "generator_function": "tf_repositories",
                    "sha256": "d0833bc6dad817a367936a5f902a0c11318160b5e80a20ece35fb85a5675c886",
                    "urls": [
                        "https://storage.googleapis.com/mirror.tensorflow.org/github.com/bazelbuild/rules_swift/archive/3eeeb53cebda55b349d64c9fc144e18c5f7c0eb8.tar.gz",
                        "https://github.com/bazelbuild/rules_swift/archive/3eeeb53cebda55b349d64c9fc144e18c5f7c0eb8.tar.gz"
                    ],
                    "strip_prefix": "rules_swift-3eeeb53cebda55b349d64c9fc144e18c5f7c0eb8"
                },
                "output_tree_hash": "25e61d82e99377f169e540209be2537698adcdceec26b15ff6643014415b74ff"
            }
        ]
    },
    {
        "original_rule_class": "//third_party/android:android_configure.bzl%android_configure",
        "definition_information": "Repository local_config_android instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule android_configure defined at:\n  /home/abuild/rpmbuild/BUILD/tensorflow-2.3.1/third_party/android/android_configure.bzl:76:36: in <toplevel>\n",
        "original_attributes": {
            "name": "local_config_android"
        },
        "repositories": [
            {
                "rule_class": "//third_party/android:android_configure.bzl%android_configure",
                "attributes": {
                    "name": "local_config_android"
                },
                "output_tree_hash": "d3c6fd2715da48480fee25015f46c2cd84ccf6771e7768ab27865955aa9eb2e4"
            }
        ]
    },
    {
        "original_rule_class": "//third_party:repo.bzl%tf_http_archive",
        "definition_information": "Repository com_github_grpc_grpc instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule tf_http_archive defined at:\n  /home/abuild/rpmbuild/BUILD/tensorflow-2.3.1/third_party/repo.bzl:134:34: in <toplevel>\n",
        "original_attributes": {
            "name": "com_github_grpc_grpc",
            "generator_name": "com_github_grpc_grpc",
            "generator_function": "tf_repositories",
            "sha256": "b956598d8cbe168b5ee717b5dafa56563eb5201a947856a6688bbeac9cac4e1f",
            "urls": [
                "https://storage.googleapis.com/mirror.tensorflow.org/github.com/grpc/grpc/archive/b54a5b338637f92bfcf4b0bc05e0f57a5fd8fadd.tar.gz",
                "https://github.com/grpc/grpc/archive/b54a5b338637f92bfcf4b0bc05e0f57a5fd8fadd.tar.gz"
            ],
            "strip_prefix": "grpc-b54a5b338637f92bfcf4b0bc05e0f57a5fd8fadd",
            "system_build_file": "//third_party/systemlibs:grpc.BUILD",
            "system_link_files": {
                "//third_party/systemlibs:BUILD": "bazel/BUILD",
                "//third_party/systemlibs:grpc.BUILD": "src/compiler/BUILD",
                "//third_party/systemlibs:grpc.bazel.grpc_deps.bzl": "bazel/grpc_deps.bzl"
            }
        },
        "repositories": [
            {
                "rule_class": "//third_party:repo.bzl%tf_http_archive",
                "attributes": {
                    "name": "com_github_grpc_grpc",
                    "generator_name": "com_github_grpc_grpc",
                    "generator_function": "tf_repositories",
                    "sha256": "b956598d8cbe168b5ee717b5dafa56563eb5201a947856a6688bbeac9cac4e1f",
                    "urls": [
                        "https://storage.googleapis.com/mirror.tensorflow.org/github.com/grpc/grpc/archive/b54a5b338637f92bfcf4b0bc05e0f57a5fd8fadd.tar.gz",
                        "https://github.com/grpc/grpc/archive/b54a5b338637f92bfcf4b0bc05e0f57a5fd8fadd.tar.gz"
                    ],
                    "strip_prefix": "grpc-b54a5b338637f92bfcf4b0bc05e0f57a5fd8fadd",
                    "system_build_file": "//third_party/systemlibs:grpc.BUILD",
                    "system_link_files": {
                        "//third_party/systemlibs:BUILD": "bazel/BUILD",
                        "//third_party/systemlibs:grpc.BUILD": "src/compiler/BUILD",
                        "//third_party/systemlibs:grpc.bazel.grpc_deps.bzl": "bazel/grpc_deps.bzl"
                    }
                },
                "output_tree_hash": "19ed8c6909b9c12de1f84a88b8cbbc672b5036fdd35b9aa3915dffcff505da59"
            }
        ]
    },
    {
        "original_rule_class": "//third_party:repo.bzl%tf_http_archive",
        "definition_information": "Repository build_bazel_rules_apple instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule tf_http_archive defined at:\n  /home/abuild/rpmbuild/BUILD/tensorflow-2.3.1/third_party/repo.bzl:134:34: in <toplevel>\n",
        "original_attributes": {
            "name": "build_bazel_rules_apple",
            "generator_name": "build_bazel_rules_apple",
            "generator_function": "tf_repositories",
            "sha256": "ee9e6073aeb5a65c100cb9c44b0017c937706a4ae03176e14a7e78620a198079",
            "urls": [
                "https://storage.googleapis.com/mirror.tensorflow.org/github.com/bazelbuild/rules_apple/archive/5131f3d46794bf227d296c82f30c2499c9de3c5b.tar.gz",
                "https://github.com/bazelbuild/rules_apple/archive/5131f3d46794bf227d296c82f30c2499c9de3c5b.tar.gz"
            ],
            "strip_prefix": "rules_apple-5131f3d46794bf227d296c82f30c2499c9de3c5b"
        },
        "repositories": [
            {
                "rule_class": "//third_party:repo.bzl%tf_http_archive",
                "attributes": {
                    "name": "build_bazel_rules_apple",
                    "generator_name": "build_bazel_rules_apple",
                    "generator_function": "tf_repositories",
                    "sha256": "ee9e6073aeb5a65c100cb9c44b0017c937706a4ae03176e14a7e78620a198079",
                    "urls": [
                        "https://storage.googleapis.com/mirror.tensorflow.org/github.com/bazelbuild/rules_apple/archive/5131f3d46794bf227d296c82f30c2499c9de3c5b.tar.gz",
                        "https://github.com/bazelbuild/rules_apple/archive/5131f3d46794bf227d296c82f30c2499c9de3c5b.tar.gz"
                    ],
                    "strip_prefix": "rules_apple-5131f3d46794bf227d296c82f30c2499c9de3c5b"
                },
                "output_tree_hash": "9c4a48590274316cf6ac13864a24c2e66da2a57f9b0940b501497d84ae70ef6b"
            }
        ]
    },
    {
        "original_rule_class": "//third_party:repo.bzl%tf_http_archive",
        "definition_information": "Repository build_bazel_apple_support instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule tf_http_archive defined at:\n  /home/abuild/rpmbuild/BUILD/tensorflow-2.3.1/third_party/repo.bzl:134:34: in <toplevel>\n",
        "original_attributes": {
            "name": "build_bazel_apple_support",
            "generator_name": "build_bazel_apple_support",
            "generator_function": "tf_repositories",
            "sha256": "ad8ae80e93612b8151019367a3d1604d7a51c14480dae1254e10252007e8260c",
            "urls": [
                "https://storage.googleapis.com/mirror.tensorflow.org/github.com/bazelbuild/apple_support/archive/501b4afb27745c4813a88ffa28acd901408014e4.tar.gz",
                "https://github.com/bazelbuild/apple_support/archive/501b4afb27745c4813a88ffa28acd901408014e4.tar.gz"
            ],
            "strip_prefix": "apple_support-501b4afb27745c4813a88ffa28acd901408014e4"
        },
        "repositories": [
            {
                "rule_class": "//third_party:repo.bzl%tf_http_archive",
                "attributes": {
                    "name": "build_bazel_apple_support",
                    "generator_name": "build_bazel_apple_support",
                    "generator_function": "tf_repositories",
                    "sha256": "ad8ae80e93612b8151019367a3d1604d7a51c14480dae1254e10252007e8260c",
                    "urls": [
                        "https://storage.googleapis.com/mirror.tensorflow.org/github.com/bazelbuild/apple_support/archive/501b4afb27745c4813a88ffa28acd901408014e4.tar.gz",
                        "https://github.com/bazelbuild/apple_support/archive/501b4afb27745c4813a88ffa28acd901408014e4.tar.gz"
                    ],
                    "strip_prefix": "apple_support-501b4afb27745c4813a88ffa28acd901408014e4"
                },
                "output_tree_hash": "f389619efdde1bf9a7433e404bc60f946a6f827d1fcf3674d1aa3a671f16eaa9"
            }
        ]
    },
    {
        "original_rule_class": "@bazel_tools//tools/build_defs/repo:http.bzl%http_archive",
        "definition_information": "Repository upb instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule http_archive defined at:\n  /home/abuild/rpmbuild/BUILD/output_user_root/55287d6e880b1ebc99135e2e30fc9816/external/bazel_tools/tools/build_defs/repo/http.bzl:336:31: in <toplevel>\n",
        "original_attributes": {
            "name": "upb",
            "generator_name": "upb",
            "generator_function": "grpc_deps",
            "url": "https://github.com/protocolbuffers/upb/archive/9effcbcb27f0a665f9f345030188c0b291e32482.tar.gz",
            "sha256": "61d0417abd60e65ed589c9deee7c124fe76a4106831f6ad39464e1525cef1454",
            "strip_prefix": "upb-9effcbcb27f0a665f9f345030188c0b291e32482"
        },
        "repositories": [
            {
                "rule_class": "@bazel_tools//tools/build_defs/repo:http.bzl%http_archive",
                "attributes": {
                    "url": "https://github.com/protocolbuffers/upb/archive/9effcbcb27f0a665f9f345030188c0b291e32482.tar.gz",
                    "urls": [],
                    "sha256": "61d0417abd60e65ed589c9deee7c124fe76a4106831f6ad39464e1525cef1454",
                    "netrc": "",
                    "auth_patterns": {},
                    "canonical_id": "",
                    "strip_prefix": "upb-9effcbcb27f0a665f9f345030188c0b291e32482",
                    "type": "",
                    "patches": [],
                    "patch_tool": "",
                    "patch_args": [
                        "-p0"
                    ],
                    "patch_cmds": [],
                    "patch_cmds_win": [],
                    "build_file_content": "",
                    "workspace_file_content": "",
                    "name": "upb"
                },
                "output_tree_hash": "a72ebe29af1555e3c503692a295ffb206967e4db87e1f8470eea28c1ea13f90e"
            }
        ]
    },
    {
        "original_rule_class": "//third_party:repo.bzl%tf_http_archive",
        "definition_information": "Repository com_google_googleapis instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule tf_http_archive defined at:\n  /home/abuild/rpmbuild/BUILD/tensorflow-2.3.1/third_party/repo.bzl:134:34: in <toplevel>\n",
        "original_attributes": {
            "name": "com_google_googleapis",
            "generator_name": "com_google_googleapis",
            "generator_function": "tf_repositories",
            "sha256": "7ebab01b06c555f4b6514453dc3e1667f810ef91d1d4d2d3aa29bb9fcb40a900",
            "urls": [
                "https://storage.googleapis.com/mirror.tensorflow.org/github.com/googleapis/googleapis/archive/541b1ded4abadcc38e8178680b0677f65594ea6f.zip",
                "https://github.com/googleapis/googleapis/archive/541b1ded4abadcc38e8178680b0677f65594ea6f.zip"
            ],
            "strip_prefix": "googleapis-541b1ded4abadcc38e8178680b0677f65594ea6f",
            "build_file": "//third_party/googleapis:googleapis.BUILD"
        },
        "repositories": [
            {
                "rule_class": "//third_party:repo.bzl%tf_http_archive",
                "attributes": {
                    "name": "com_google_googleapis",
                    "generator_name": "com_google_googleapis",
                    "generator_function": "tf_repositories",
                    "sha256": "7ebab01b06c555f4b6514453dc3e1667f810ef91d1d4d2d3aa29bb9fcb40a900",
                    "urls": [
                        "https://storage.googleapis.com/mirror.tensorflow.org/github.com/googleapis/googleapis/archive/541b1ded4abadcc38e8178680b0677f65594ea6f.zip",
                        "https://github.com/googleapis/googleapis/archive/541b1ded4abadcc38e8178680b0677f65594ea6f.zip"
                    ],
                    "strip_prefix": "googleapis-541b1ded4abadcc38e8178680b0677f65594ea6f",
                    "build_file": "//third_party/googleapis:googleapis.BUILD"
                },
                "output_tree_hash": "6f6a6e242ca2f1114eb244e899d6ded23909f7919163a8fc6ccf7badd0395d44"
            }
        ]
    },
    {
        "original_rule_class": "//third_party/systemlibs:syslibs_configure.bzl%syslibs_configure",
        "definition_information": "Repository local_config_syslibs instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule syslibs_configure defined at:\n  /home/abuild/rpmbuild/BUILD/tensorflow-2.3.1/third_party/systemlibs/syslibs_configure.bzl:154:36: in <toplevel>\n",
        "original_attributes": {
            "name": "local_config_syslibs",
            "generator_name": "local_config_syslibs",
            "generator_function": "tf_repositories"
        },
        "repositories": [
            {
                "rule_class": "//third_party/systemlibs:syslibs_configure.bzl%syslibs_configure",
                "attributes": {
                    "name": "local_config_syslibs",
                    "generator_name": "local_config_syslibs",
                    "generator_function": "tf_repositories"
                },
                "output_tree_hash": "c13a0b605f549e8dd75de1eddc78e72fe47da6237a2ebaabd401ece398eb6943"
            }
        ]
    },
    {
        "original_rule_class": "//third_party/tensorrt:tensorrt_configure.bzl%tensorrt_configure",
        "definition_information": "Repository local_config_tensorrt instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule tensorrt_configure defined at:\n  /home/abuild/rpmbuild/BUILD/tensorflow-2.3.1/third_party/tensorrt/tensorrt_configure.bzl:197:37: in <toplevel>\n",
        "original_attributes": {
            "name": "local_config_tensorrt",
            "generator_name": "local_config_tensorrt",
            "generator_function": "tf_repositories"
        },
        "repositories": [
            {
                "rule_class": "//third_party/tensorrt:tensorrt_configure.bzl%tensorrt_configure",
                "attributes": {
                    "name": "local_config_tensorrt",
                    "generator_name": "local_config_tensorrt",
                    "generator_function": "tf_repositories"
                },
                "output_tree_hash": "cde52031e1db8d9c00ac7382ae48dbe0e8ce80336e913b4e70c804c3a376857f"
            }
        ]
    },
    {
        "original_rule_class": "//third_party/toolchains/remote:configure.bzl%remote_execution_configure",
        "definition_information": "Repository local_config_remote_execution instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule remote_execution_configure defined at:\n  /home/abuild/rpmbuild/BUILD/tensorflow-2.3.1/third_party/toolchains/remote/configure.bzl:40:45: in <toplevel>\n",
        "original_attributes": {
            "name": "local_config_remote_execution",
            "generator_name": "local_config_remote_execution",
            "generator_function": "tf_repositories"
        },
        "repositories": [
            {
                "rule_class": "//third_party/toolchains/remote:configure.bzl%remote_execution_configure",
                "attributes": {
                    "name": "local_config_remote_execution",
                    "generator_name": "local_config_remote_execution",
                    "generator_function": "tf_repositories"
                },
                "output_tree_hash": "e73b1f4b0799c1d92c9e9f18e7f965c712ef58b3368647243b1df139c7db935a"
            }
        ]
    },
    {
        "original_rule_class": "//third_party/gpus:rocm_configure.bzl%rocm_configure",
        "definition_information": "Repository local_config_rocm instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule rocm_configure defined at:\n  /home/abuild/rpmbuild/BUILD/tensorflow-2.3.1/third_party/gpus/rocm_configure.bzl:866:33: in <toplevel>\n",
        "original_attributes": {
            "name": "local_config_rocm",
            "generator_name": "local_config_rocm",
            "generator_function": "tf_repositories"
        },
        "repositories": [
            {
                "rule_class": "//third_party/gpus:rocm_configure.bzl%rocm_configure",
                "attributes": {
                    "name": "local_config_rocm",
                    "generator_name": "local_config_rocm",
                    "generator_function": "tf_repositories"
                },
                "output_tree_hash": "0d870155343893d9e147eb9ee373f39e83840a726732ddb8bdbe30e5036220ff"
            }
        ]
    },
    {
        "original_rule_class": "//third_party/gpus:cuda_configure.bzl%cuda_configure",
        "definition_information": "Repository local_config_cuda instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule cuda_configure defined at:\n  /home/abuild/rpmbuild/BUILD/tensorflow-2.3.1/third_party/gpus/cuda_configure.bzl:1399:33: in <toplevel>\n",
        "original_attributes": {
            "name": "local_config_cuda",
            "generator_name": "local_config_cuda",
            "generator_function": "tf_repositories"
        },
        "repositories": [
            {
                "rule_class": "//third_party/gpus:cuda_configure.bzl%cuda_configure",
                "attributes": {
                    "name": "local_config_cuda",
                    "generator_name": "local_config_cuda",
                    "generator_function": "tf_repositories"
                },
                "output_tree_hash": "0cd7fb07ccab096d5f4a072696a17ce1aa68e35919a493d6b6552409865c2ecd"
            }
        ]
    },
    {
        "original_rule_class": "//third_party:repo.bzl%tf_http_archive",
        "definition_information": "Repository rules_cc instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule tf_http_archive defined at:\n  /home/abuild/rpmbuild/BUILD/tensorflow-2.3.1/third_party/repo.bzl:134:34: in <toplevel>\n",
        "original_attributes": {
            "name": "rules_cc",
            "generator_name": "rules_cc",
            "generator_function": "tf_repositories",
            "sha256": "cf3b76a90c86c0554c5b10f4b160f05af71d252026b71362c4674e2fb9936cf9",
            "urls": [
                "https://storage.googleapis.com/mirror.tensorflow.org/github.com/bazelbuild/rules_cc/archive/01d4a48911d5e7591ecb1c06d3b8af47fe872371.zip",
                "https://github.com/bazelbuild/rules_cc/archive/01d4a48911d5e7591ecb1c06d3b8af47fe872371.zip"
            ],
            "strip_prefix": "rules_cc-01d4a48911d5e7591ecb1c06d3b8af47fe872371"
        },
        "repositories": [
            {
                "rule_class": "//third_party:repo.bzl%tf_http_archive",
                "attributes": {
                    "name": "rules_cc",
                    "generator_name": "rules_cc",
                    "generator_function": "tf_repositories",
                    "sha256": "cf3b76a90c86c0554c5b10f4b160f05af71d252026b71362c4674e2fb9936cf9",
                    "urls": [
                        "https://storage.googleapis.com/mirror.tensorflow.org/github.com/bazelbuild/rules_cc/archive/01d4a48911d5e7591ecb1c06d3b8af47fe872371.zip",
                        "https://github.com/bazelbuild/rules_cc/archive/01d4a48911d5e7591ecb1c06d3b8af47fe872371.zip"
                    ],
                    "strip_prefix": "rules_cc-01d4a48911d5e7591ecb1c06d3b8af47fe872371"
                },
                "output_tree_hash": "0e8f8ce2bff33067f744d51013b85dd7e74043b5cb31edec618e645f7e7d5d9a"
            }
        ]
    },
    {
        "original_rule_class": "local_config_platform",
        "original_attributes": {
            "name": "local_config_platform"
        },
        "native": "local_config_platform(name = 'local_config_platform')"
    },
    {
        "original_rule_class": "local_repository",
        "original_attributes": {
            "name": "platforms",
            "path": "/home/abuild/rpmbuild/BUILD/output_user_root/install/f520b02a0d98143caf8e25678d03643a/platforms"
        },
        "native": "local_repository(name = \"platforms\", path = __embedded_dir__ + \"/\" + \"platforms\")"
    },
    {
        "original_rule_class": "local_repository",
        "original_attributes": {
            "name": "org_tensorflow",
            "path": "."
        },
        "native": "local_repository(name = \"org_tensorflow\", path = \".\")"
    },
    {
        "original_rule_class": "//third_party/remote_config:remote_platform_configure.bzl%remote_platform_configure",
        "definition_information": "Repository local_execution_config_platform instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule remote_platform_configure defined at:\n  /home/abuild/rpmbuild/BUILD/tensorflow-2.3.1/third_party/remote_config/remote_platform_configure.bzl:43:44: in <toplevel>\n",
        "original_attributes": {
            "name": "local_execution_config_platform",
            "generator_name": "local_execution_config_platform",
            "generator_function": "tf_repositories",
            "platform_exec_properties": {},
            "platform": "local"
        },
        "repositories": [
            {
                "rule_class": "//third_party/remote_config:remote_platform_configure.bzl%remote_platform_configure",
                "attributes": {
                    "name": "local_execution_config_platform",
                    "generator_name": "local_execution_config_platform",
                    "generator_function": "tf_repositories",
                    "platform_exec_properties": {},
                    "platform": "local"
                },
                "output_tree_hash": "7474dbcd4dc47fccf412f2cb0f36eb8fc37995acbce3c6e493d573daec8134b3"
            }
        ]
    },
    {
        "original_rule_class": "//third_party:repo.bzl%tf_http_archive",
        "definition_information": "Repository com_google_protobuf instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule tf_http_archive defined at:\n  /home/abuild/rpmbuild/BUILD/tensorflow-2.3.1/third_party/repo.bzl:134:34: in <toplevel>\n",
        "original_attributes": {
            "name": "com_google_protobuf",
            "generator_name": "com_google_protobuf",
            "generator_function": "tf_repositories",
            "sha256": "cfcba2df10feec52a84208693937c17a4b5df7775e1635c1e3baffc487b24c9b",
            "urls": [
                "https://storage.googleapis.com/mirror.tensorflow.org/github.com/protocolbuffers/protobuf/archive/v3.9.2.zip",
                "https://github.com/protocolbuffers/protobuf/archive/v3.9.2.zip"
            ],
            "strip_prefix": "protobuf-3.9.2",
            "patch_file": "//third_party/protobuf:protobuf.patch",
            "system_build_file": "//third_party/systemlibs:protobuf.BUILD",
            "system_link_files": {
                "//third_party/systemlibs:protobuf.bzl": "protobuf.bzl"
            }
        },
        "repositories": [
            {
                "rule_class": "//third_party:repo.bzl%tf_http_archive",
                "attributes": {
                    "name": "com_google_protobuf",
                    "generator_name": "com_google_protobuf",
                    "generator_function": "tf_repositories",
                    "sha256": "cfcba2df10feec52a84208693937c17a4b5df7775e1635c1e3baffc487b24c9b",
                    "urls": [
                        "https://storage.googleapis.com/mirror.tensorflow.org/github.com/protocolbuffers/protobuf/archive/v3.9.2.zip",
                        "https://github.com/protocolbuffers/protobuf/archive/v3.9.2.zip"
                    ],
                    "strip_prefix": "protobuf-3.9.2",
                    "patch_file": "//third_party/protobuf:protobuf.patch",
                    "system_build_file": "//third_party/systemlibs:protobuf.BUILD",
                    "system_link_files": {
                        "//third_party/systemlibs:protobuf.bzl": "protobuf.bzl"
                    }
                },
                "output_tree_hash": "4d617c354b374674526a969d27d87d2a2ee06477e75fcefbe0d4dcbb8257e411"
            }
        ]
    },
    {
        "original_rule_class": "//third_party/sycl:sycl_configure.bzl%sycl_configure",
        "definition_information": "Repository local_config_sycl instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule sycl_configure defined at:\n  /home/abuild/rpmbuild/BUILD/tensorflow-2.3.1/third_party/sycl/sycl_configure.bzl:246:33: in <toplevel>\n",
        "original_attributes": {
            "name": "local_config_sycl",
            "generator_name": "local_config_sycl",
            "generator_function": "tf_repositories"
        },
        "repositories": [
            {
                "rule_class": "//third_party/sycl:sycl_configure.bzl%sycl_configure",
                "attributes": {
                    "name": "local_config_sycl",
                    "generator_name": "local_config_sycl",
                    "generator_function": "tf_repositories"
                },
                "output_tree_hash": "9672b8697f4ed637800f752d2c0dbf7921f8a5a69c0b8f20aa8060ddeff2002c"
            }
        ]
    },
    {
        "original_rule_class": "//third_party:repo.bzl%third_party_http_archive",
        "definition_information": "Repository flatbuffers instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule third_party_http_archive defined at:\n  /home/abuild/rpmbuild/BUILD/tensorflow-2.3.1/third_party/repo.bzl:219:43: in <toplevel>\n",
        "original_attributes": {
            "name": "flatbuffers",
            "generator_name": "flatbuffers",
            "generator_function": "tf_repositories",
            "sha256": "62f2223fb9181d1d6338451375628975775f7522185266cd5296571ac152bc45",
            "urls": [
                "https://storage.googleapis.com/mirror.tensorflow.org/github.com/google/flatbuffers/archive/v1.12.0.tar.gz",
                "https://github.com/google/flatbuffers/archive/v1.12.0.tar.gz"
            ],
            "strip_prefix": "flatbuffers-1.12.0",
            "delete": [
                "build_defs.bzl"
            ],
            "build_file": "//third_party/flatbuffers:BUILD.bazel",
            "system_build_file": "//third_party/flatbuffers:BUILD.system",
            "link_files": {
                "//third_party/flatbuffers:build_defs.bzl": "build_defs.bzl"
            }
        },
        "repositories": [
            {
                "rule_class": "//third_party:repo.bzl%third_party_http_archive",
                "attributes": {
                    "name": "flatbuffers",
                    "generator_name": "flatbuffers",
                    "generator_function": "tf_repositories",
                    "sha256": "62f2223fb9181d1d6338451375628975775f7522185266cd5296571ac152bc45",
                    "urls": [
                        "https://storage.googleapis.com/mirror.tensorflow.org/github.com/google/flatbuffers/archive/v1.12.0.tar.gz",
                        "https://github.com/google/flatbuffers/archive/v1.12.0.tar.gz"
                    ],
                    "strip_prefix": "flatbuffers-1.12.0",
                    "delete": [
                        "build_defs.bzl"
                    ],
                    "build_file": "//third_party/flatbuffers:BUILD.bazel",
                    "system_build_file": "//third_party/flatbuffers:BUILD.system",
                    "link_files": {
                        "//third_party/flatbuffers:build_defs.bzl": "build_defs.bzl"
                    }
                },
                "output_tree_hash": "50d0bb5627b8c97c1297f4934243b5eebc3c366af4ef8d8a5c8b3604eefbad1d"
            }
        ]
    },
    {
        "original_rule_class": "//third_party:repo.bzl%tf_http_archive",
        "definition_information": "Repository build_bazel_rules_android instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule tf_http_archive defined at:\n  /home/abuild/rpmbuild/BUILD/tensorflow-2.3.1/third_party/repo.bzl:134:34: in <toplevel>\n",
        "original_attributes": {
            "name": "build_bazel_rules_android",
            "generator_name": "build_bazel_rules_android",
            "generator_function": "tf_repositories",
            "sha256": "cd06d15dd8bb59926e4d65f9003bfc20f9da4b2519985c27e190cddc8b7a7806",
            "urls": [
                "https://storage.googleapis.com/mirror.tensorflow.org/github.com/bazelbuild/rules_android/archive/v0.1.1.zip",
                "https://github.com/bazelbuild/rules_android/archive/v0.1.1.zip"
            ],
            "strip_prefix": "rules_android-0.1.1"
        },
        "repositories": [
            {
                "rule_class": "//third_party:repo.bzl%tf_http_archive",
                "attributes": {
                    "name": "build_bazel_rules_android",
                    "generator_name": "build_bazel_rules_android",
                    "generator_function": "tf_repositories",
                    "sha256": "cd06d15dd8bb59926e4d65f9003bfc20f9da4b2519985c27e190cddc8b7a7806",
                    "urls": [
                        "https://storage.googleapis.com/mirror.tensorflow.org/github.com/bazelbuild/rules_android/archive/v0.1.1.zip",
                        "https://github.com/bazelbuild/rules_android/archive/v0.1.1.zip"
                    ],
                    "strip_prefix": "rules_android-0.1.1"
                },
                "output_tree_hash": "3e5f234e84ea3274cc89c009be070795b451fa25db0323a32ac56fc13cad004d"
            }
        ]
    },
    {
        "original_rule_class": "@bazel_tools//tools/cpp:cc_configure.bzl%cc_autoconf_toolchains",
        "definition_information": "Repository local_config_cc_toolchains instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule cc_autoconf_toolchains defined at:\n  /home/abuild/rpmbuild/BUILD/output_user_root/55287d6e880b1ebc99135e2e30fc9816/external/bazel_tools/tools/cpp/cc_configure.bzl:79:41: in <toplevel>\n",
        "original_attributes": {
            "name": "local_config_cc_toolchains",
            "generator_name": "local_config_cc_toolchains",
            "generator_function": "cc_configure"
        },
        "repositories": [
            {
                "rule_class": "@bazel_tools//tools/cpp:cc_configure.bzl%cc_autoconf_toolchains",
                "attributes": {
                    "name": "local_config_cc_toolchains",
                    "generator_name": "local_config_cc_toolchains",
                    "generator_function": "cc_configure"
                },
                "output_tree_hash": "1f5225797781e52701eedc83d3881885dbf142cf22222c8ef3b38c8a4b70070e"
            }
        ]
    },
    {
        "original_rule_class": "@bazel_tools//tools/sh:sh_configure.bzl%sh_config",
        "definition_information": "Repository local_config_sh instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule sh_config defined at:\n  /home/abuild/rpmbuild/BUILD/output_user_root/55287d6e880b1ebc99135e2e30fc9816/external/bazel_tools/tools/sh/sh_configure.bzl:72:28: in <toplevel>\n",
        "original_attributes": {
            "name": "local_config_sh",
            "generator_name": "local_config_sh",
            "generator_function": "sh_configure"
        },
        "repositories": [
            {
                "rule_class": "@bazel_tools//tools/sh:sh_configure.bzl%sh_config",
                "attributes": {
                    "name": "local_config_sh",
                    "generator_name": "local_config_sh",
                    "generator_function": "sh_configure"
                },
                "output_tree_hash": "7bf5ba89669bcdf4526f821bc9f1f9f49409ae9c61c4e8f21c9f17e06c475127"
            }
        ]
    },
    {
        "original_rule_class": "//third_party:repo.bzl%tf_http_archive",
        "definition_information": "Repository six_archive instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule tf_http_archive defined at:\n  /home/abuild/rpmbuild/BUILD/tensorflow-2.3.1/third_party/repo.bzl:134:34: in <toplevel>\n",
        "original_attributes": {
            "name": "six_archive",
            "generator_name": "six_archive",
            "generator_function": "tf_repositories",
            "sha256": "d16a0141ec1a18405cd4ce8b4613101da75da0e9a7aec5bdd4fa804d0e0eba73",
            "urls": [
                "https://storage.googleapis.com/mirror.tensorflow.org/pypi.python.org/packages/source/s/six/six-1.12.0.tar.gz",
                "https://pypi.python.org/packages/source/s/six/six-1.12.0.tar.gz"
            ],
            "strip_prefix": "six-1.12.0",
            "build_file": "//third_party:six.BUILD",
            "system_build_file": "//third_party/systemlibs:six.BUILD"
        },
        "repositories": [
            {
                "rule_class": "//third_party:repo.bzl%tf_http_archive",
                "attributes": {
                    "name": "six_archive",
                    "generator_name": "six_archive",
                    "generator_function": "tf_repositories",
                    "sha256": "d16a0141ec1a18405cd4ce8b4613101da75da0e9a7aec5bdd4fa804d0e0eba73",
                    "urls": [
                        "https://storage.googleapis.com/mirror.tensorflow.org/pypi.python.org/packages/source/s/six/six-1.12.0.tar.gz",
                        "https://pypi.python.org/packages/source/s/six/six-1.12.0.tar.gz"
                    ],
                    "strip_prefix": "six-1.12.0",
                    "build_file": "//third_party:six.BUILD",
                    "system_build_file": "//third_party/systemlibs:six.BUILD"
                },
                "output_tree_hash": "e5f8052162cd0dbba54ce187b4909b58373547ddf022f16ced5a1ce842eeef2d"
            }
        ]
    },
    {
        "original_rule_class": "//third_party:repo.bzl%tf_http_archive",
        "definition_information": "Repository wrapt instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule tf_http_archive defined at:\n  /home/abuild/rpmbuild/BUILD/tensorflow-2.3.1/third_party/repo.bzl:134:34: in <toplevel>\n",
        "original_attributes": {
            "name": "wrapt",
            "generator_name": "wrapt",
            "generator_function": "tf_repositories",
            "sha256": "8a6fb40e8f8b6a66b4ba81a4044c68e6a7b1782f21cfabc06fb765332b4c3e51",
            "urls": [
                "https://storage.googleapis.com/mirror.tensorflow.org/github.com/GrahamDumpleton/wrapt/archive/1.11.1.tar.gz",
                "https://github.com/GrahamDumpleton/wrapt/archive/1.11.1.tar.gz"
            ],
            "strip_prefix": "wrapt-1.11.1/src/wrapt",
            "build_file": "//third_party:wrapt.BUILD",
            "system_build_file": "//third_party/systemlibs:wrapt.BUILD"
        },
        "repositories": [
            {
                "rule_class": "//third_party:repo.bzl%tf_http_archive",
                "attributes": {
                    "name": "wrapt",
                    "generator_name": "wrapt",
                    "generator_function": "tf_repositories",
                    "sha256": "8a6fb40e8f8b6a66b4ba81a4044c68e6a7b1782f21cfabc06fb765332b4c3e51",
                    "urls": [
                        "https://storage.googleapis.com/mirror.tensorflow.org/github.com/GrahamDumpleton/wrapt/archive/1.11.1.tar.gz",
                        "https://github.com/GrahamDumpleton/wrapt/archive/1.11.1.tar.gz"
                    ],
                    "strip_prefix": "wrapt-1.11.1/src/wrapt",
                    "build_file": "//third_party:wrapt.BUILD",
                    "system_build_file": "//third_party/systemlibs:wrapt.BUILD"
                },
                "output_tree_hash": "38afa3102554b8fd849df18cca5ccc1c020f2e2c27f4f7c7b7a1ee2fae18b8f9"
            }
        ]
    },
    {
        "original_rule_class": "//third_party:repo.bzl%tf_http_archive",
        "definition_information": "Repository absl_py instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule tf_http_archive defined at:\n  /home/abuild/rpmbuild/BUILD/tensorflow-2.3.1/third_party/repo.bzl:134:34: in <toplevel>\n",
        "original_attributes": {
            "name": "absl_py",
            "generator_name": "absl_py",
            "generator_function": "tf_repositories",
            "sha256": "603febc9b95a8f2979a7bdb77d2f5e4d9b30d4e0d59579f88eba67d4e4cc5462",
            "urls": [
                "https://storage.googleapis.com/mirror.tensorflow.org/github.com/abseil/abseil-py/archive/pypi-v0.9.0.tar.gz",
                "https://github.com/abseil/abseil-py/archive/pypi-v0.9.0.tar.gz"
            ],
            "strip_prefix": "abseil-py-pypi-v0.9.0",
            "system_build_file": "//third_party/systemlibs:absl_py.BUILD",
            "system_link_files": {
                "//third_party/systemlibs:absl_py.absl.BUILD": "absl/BUILD",
                "//third_party/systemlibs:absl_py.absl.flags.BUILD": "absl/flags/BUILD",
                "//third_party/systemlibs:absl_py.absl.testing.BUILD": "absl/testing/BUILD"
            }
        },
        "repositories": [
            {
                "rule_class": "//third_party:repo.bzl%tf_http_archive",
                "attributes": {
                    "name": "absl_py",
                    "generator_name": "absl_py",
                    "generator_function": "tf_repositories",
                    "sha256": "603febc9b95a8f2979a7bdb77d2f5e4d9b30d4e0d59579f88eba67d4e4cc5462",
                    "urls": [
                        "https://storage.googleapis.com/mirror.tensorflow.org/github.com/abseil/abseil-py/archive/pypi-v0.9.0.tar.gz",
                        "https://github.com/abseil/abseil-py/archive/pypi-v0.9.0.tar.gz"
                    ],
                    "strip_prefix": "abseil-py-pypi-v0.9.0",
                    "system_build_file": "//third_party/systemlibs:absl_py.BUILD",
                    "system_link_files": {
                        "//third_party/systemlibs:absl_py.absl.BUILD": "absl/BUILD",
                        "//third_party/systemlibs:absl_py.absl.flags.BUILD": "absl/flags/BUILD",
                        "//third_party/systemlibs:absl_py.absl.testing.BUILD": "absl/testing/BUILD"
                    }
                },
                "output_tree_hash": "cc8bed465e12bf8abda56a883e12ea8a9ca69c6493b07d6ba9cf2f4b143bd9a4"
            }
        ]
    },
    {
        "original_rule_class": "//third_party:repo.bzl%tf_http_archive",
        "definition_information": "Repository gast_archive instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule tf_http_archive defined at:\n  /home/abuild/rpmbuild/BUILD/tensorflow-2.3.1/third_party/repo.bzl:134:34: in <toplevel>\n",
        "original_attributes": {
            "name": "gast_archive",
            "generator_name": "gast_archive",
            "generator_function": "tf_repositories",
            "sha256": "b881ef288a49aa81440d2c5eb8aeefd4c2bb8993d5f50edae7413a85bfdb3b57",
            "urls": [
                "http://mirror.tensorflow.org/files.pythonhosted.org/packages/12/59/eaa15ab9710a20e22225efd042cd2d6a0b559a0656d5baba9641a2a4a921/gast-0.3.3.tar.gz",
                "https://files.pythonhosted.org/packages/12/59/eaa15ab9710a20e22225efd042cd2d6a0b559a0656d5baba9641a2a4a921/gast-0.3.3.tar.gz"
            ],
            "strip_prefix": "gast-0.3.3",
            "build_file": "//third_party:gast.BUILD",
            "system_build_file": "//third_party/systemlibs:gast.BUILD"
        },
        "repositories": [
            {
                "rule_class": "//third_party:repo.bzl%tf_http_archive",
                "attributes": {
                    "name": "gast_archive",
                    "generator_name": "gast_archive",
                    "generator_function": "tf_repositories",
                    "sha256": "b881ef288a49aa81440d2c5eb8aeefd4c2bb8993d5f50edae7413a85bfdb3b57",
                    "urls": [
                        "http://mirror.tensorflow.org/files.pythonhosted.org/packages/12/59/eaa15ab9710a20e22225efd042cd2d6a0b559a0656d5baba9641a2a4a921/gast-0.3.3.tar.gz",
                        "https://files.pythonhosted.org/packages/12/59/eaa15ab9710a20e22225efd042cd2d6a0b559a0656d5baba9641a2a4a921/gast-0.3.3.tar.gz"
                    ],
                    "strip_prefix": "gast-0.3.3",
                    "build_file": "//third_party:gast.BUILD",
                    "system_build_file": "//third_party/systemlibs:gast.BUILD"
                },
                "output_tree_hash": "8fb564b656244a03e3d30ab1d9d4d0707ccd1b31d3d945d1da9df5c307a7d18c"
            }
        ]
    },
    {
        "original_rule_class": "//third_party/py:python_configure.bzl%python_configure",
        "definition_information": "Repository local_config_python instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule python_configure defined at:\n  /home/abuild/rpmbuild/BUILD/tensorflow-2.3.1/third_party/py/python_configure.bzl:294:35: in <toplevel>\n",
        "original_attributes": {
            "name": "local_config_python",
            "generator_name": "local_config_python",
            "generator_function": "tf_repositories"
        },
        "repositories": [
            {
                "rule_class": "//third_party/py:python_configure.bzl%python_configure",
                "attributes": {
                    "name": "local_config_python",
                    "generator_name": "local_config_python",
                    "generator_function": "tf_repositories"
                },
                "output_tree_hash": "f90873bcdf1c2c19b603548d9527f1fc63bb71e3ef49b55ef4fd001fdaf0e676"
            }
        ]
    },
    {
        "original_rule_class": "@bazel_tools//tools/build_defs/repo:http.bzl%http_archive",
        "definition_information": "Repository rules_proto instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule http_archive defined at:\n  /home/abuild/rpmbuild/BUILD/output_user_root/55287d6e880b1ebc99135e2e30fc9816/external/bazel_tools/tools/build_defs/repo/http.bzl:336:31: in <toplevel>\n",
        "original_attributes": {
            "name": "rules_proto",
            "generator_name": "rules_proto",
            "generator_function": "maybe",
            "urls": [
                "https://mirror.bazel.build/github.com/bazelbuild/rules_proto/archive/97d8af4dc474595af3900dd85cb3a29ad28cc313.tar.gz",
                "https://github.com/bazelbuild/rules_proto/archive/97d8af4dc474595af3900dd85cb3a29ad28cc313.tar.gz"
            ],
            "sha256": "602e7161d9195e50246177e7c55b2f39950a9cf7366f74ed5f22fd45750cd208",
            "strip_prefix": "rules_proto-97d8af4dc474595af3900dd85cb3a29ad28cc313"
        },
        "repositories": [
            {
                "rule_class": "@bazel_tools//tools/build_defs/repo:http.bzl%http_archive",
                "attributes": {
                    "url": "",
                    "urls": [
                        "https://mirror.bazel.build/github.com/bazelbuild/rules_proto/archive/97d8af4dc474595af3900dd85cb3a29ad28cc313.tar.gz",
                        "https://github.com/bazelbuild/rules_proto/archive/97d8af4dc474595af3900dd85cb3a29ad28cc313.tar.gz"
                    ],
                    "sha256": "602e7161d9195e50246177e7c55b2f39950a9cf7366f74ed5f22fd45750cd208",
                    "netrc": "",
                    "auth_patterns": {},
                    "canonical_id": "",
                    "strip_prefix": "rules_proto-97d8af4dc474595af3900dd85cb3a29ad28cc313",
                    "type": "",
                    "patches": [],
                    "patch_tool": "",
                    "patch_args": [
                        "-p0"
                    ],
                    "patch_cmds": [],
                    "patch_cmds_win": [],
                    "build_file_content": "",
                    "workspace_file_content": "",
                    "name": "rules_proto"
                },
                "output_tree_hash": "4f74a84e9684fc73c48656a9a6c60bdfaed2cabdf5dc7d21da7ae2ee78c7efa3"
            }
        ]
    },
    {
        "original_rule_class": "@bazel_tools//tools/build_defs/repo:http.bzl%http_archive",
        "definition_information": "Repository rules_java instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule http_archive defined at:\n  /home/abuild/rpmbuild/BUILD/output_user_root/55287d6e880b1ebc99135e2e30fc9816/external/bazel_tools/tools/build_defs/repo/http.bzl:336:31: in <toplevel>\n",
        "original_attributes": {
            "name": "rules_java",
            "generator_name": "rules_java",
            "generator_function": "maybe",
            "urls": [
                "https://mirror.bazel.build/github.com/bazelbuild/rules_java/archive/7cf3cefd652008d0a64a419c34c13bdca6c8f178.zip",
                "https://github.com/bazelbuild/rules_java/archive/7cf3cefd652008d0a64a419c34c13bdca6c8f178.zip"
            ],
            "sha256": "bc81f1ba47ef5cc68ad32225c3d0e70b8c6f6077663835438da8d5733f917598",
            "strip_prefix": "rules_java-7cf3cefd652008d0a64a419c34c13bdca6c8f178"
        },
        "repositories": [
            {
                "rule_class": "@bazel_tools//tools/build_defs/repo:http.bzl%http_archive",
                "attributes": {
                    "url": "",
                    "urls": [
                        "https://mirror.bazel.build/github.com/bazelbuild/rules_java/archive/7cf3cefd652008d0a64a419c34c13bdca6c8f178.zip",
                        "https://github.com/bazelbuild/rules_java/archive/7cf3cefd652008d0a64a419c34c13bdca6c8f178.zip"
                    ],
                    "sha256": "bc81f1ba47ef5cc68ad32225c3d0e70b8c6f6077663835438da8d5733f917598",
                    "netrc": "",
                    "auth_patterns": {},
                    "canonical_id": "",
                    "strip_prefix": "rules_java-7cf3cefd652008d0a64a419c34c13bdca6c8f178",
                    "type": "",
                    "patches": [],
                    "patch_tool": "",
                    "patch_args": [
                        "-p0"
                    ],
                    "patch_cmds": [],
                    "patch_cmds_win": [],
                    "build_file_content": "",
                    "workspace_file_content": "",
                    "name": "rules_java"
                },
                "output_tree_hash": "00a0f1231dacff0b0cea3886200e0158c67a3600068275da14120f5434f83b5e"
            }
        ]
    },
    {
        "original_rule_class": "//third_party:repo.bzl%third_party_http_archive",
        "definition_information": "Repository sobol_data instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule third_party_http_archive defined at:\n  /home/abuild/rpmbuild/BUILD/tensorflow-2.3.1/third_party/repo.bzl:219:43: in <toplevel>\n",
        "original_attributes": {
            "name": "sobol_data",
            "generator_name": "sobol_data",
            "generator_function": "tf_repositories",
            "sha256": "583d7b975e506c076fc579d9139530596906b9195b203d42361417e9aad79b73",
            "urls": [
                "https://storage.googleapis.com/mirror.tensorflow.org/github.com/joe-kuo/sobol_data/archive/835a7d7b1ee3bc83e575e302a985c66ec4b65249.tar.gz",
                "https://github.com/joe-kuo/sobol_data/archive/835a7d7b1ee3bc83e575e302a985c66ec4b65249.tar.gz"
            ],
            "strip_prefix": "sobol_data-835a7d7b1ee3bc83e575e302a985c66ec4b65249",
            "build_file": "//third_party/sobol_data:BUILD.bazel"
        },
        "repositories": [
            {
                "rule_class": "//third_party:repo.bzl%third_party_http_archive",
                "attributes": {
                    "name": "sobol_data",
                    "generator_name": "sobol_data",
                    "generator_function": "tf_repositories",
                    "sha256": "583d7b975e506c076fc579d9139530596906b9195b203d42361417e9aad79b73",
                    "urls": [
                        "https://storage.googleapis.com/mirror.tensorflow.org/github.com/joe-kuo/sobol_data/archive/835a7d7b1ee3bc83e575e302a985c66ec4b65249.tar.gz",
                        "https://github.com/joe-kuo/sobol_data/archive/835a7d7b1ee3bc83e575e302a985c66ec4b65249.tar.gz"
                    ],
                    "strip_prefix": "sobol_data-835a7d7b1ee3bc83e575e302a985c66ec4b65249",
                    "build_file": "//third_party/sobol_data:BUILD.bazel"
                },
                "output_tree_hash": "1d5088963c5ed87b0d3ddf608b255e1bc1ab895b4bf33875c5e01bb68f4f0cc4"
            }
        ]
    },
    {
        "original_rule_class": "//third_party:repo.bzl%third_party_http_archive",
        "definition_information": "Repository ruy instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule third_party_http_archive defined at:\n  /home/abuild/rpmbuild/BUILD/tensorflow-2.3.1/third_party/repo.bzl:219:43: in <toplevel>\n",
        "original_attributes": {
            "name": "ruy",
            "generator_name": "ruy",
            "generator_function": "tf_repositories",
            "sha256": "8fd4adeeff4f29796bf7cdda64806ec0495a2435361569f02afe3fe33406f07c",
            "urls": [
                "https://storage.googleapis.com/mirror.tensorflow.org/github.com/google/ruy/archive/34ea9f4993955fa1ff4eb58e504421806b7f2e8f.zip",
                "https://github.com/google/ruy/archive/34ea9f4993955fa1ff4eb58e504421806b7f2e8f.zip"
            ],
            "strip_prefix": "ruy-34ea9f4993955fa1ff4eb58e504421806b7f2e8f",
            "build_file": "//third_party/ruy:BUILD"
        },
        "repositories": [
            {
                "rule_class": "//third_party:repo.bzl%third_party_http_archive",
                "attributes": {
                    "name": "ruy",
                    "generator_name": "ruy",
                    "generator_function": "tf_repositories",
                    "sha256": "8fd4adeeff4f29796bf7cdda64806ec0495a2435361569f02afe3fe33406f07c",
                    "urls": [
                        "https://storage.googleapis.com/mirror.tensorflow.org/github.com/google/ruy/archive/34ea9f4993955fa1ff4eb58e504421806b7f2e8f.zip",
                        "https://github.com/google/ruy/archive/34ea9f4993955fa1ff4eb58e504421806b7f2e8f.zip"
                    ],
                    "strip_prefix": "ruy-34ea9f4993955fa1ff4eb58e504421806b7f2e8f",
                    "build_file": "//third_party/ruy:BUILD"
                },
                "output_tree_hash": "5c98beb66f2d2713c696d135a288bec3416a43f98cdef256bf7f63dfc003c77c"
            }
        ]
    },
    {
        "original_rule_class": "//third_party:repo.bzl%tf_http_archive",
        "definition_information": "Repository tblib_archive instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule tf_http_archive defined at:\n  /home/abuild/rpmbuild/BUILD/tensorflow-2.3.1/third_party/repo.bzl:134:34: in <toplevel>\n",
        "original_attributes": {
            "name": "tblib_archive",
            "generator_name": "tblib_archive",
            "generator_function": "tf_repositories",
            "sha256": "436e4200e63d92316551179dc540906652878df4ff39b43db30fcf6400444fe7",
            "urls": [
                "http://mirror.tensorflow.org/files.pythonhosted.org/packages/ec/c4/8c651f3240a73c28a218194f3d527eb2be5a173d08501060cdee84ade33f/tblib-1.3.2.tar.gz",
                "https://files.pythonhosted.org/packages/ec/c4/8c651f3240a73c28a218194f3d527eb2be5a173d08501060cdee84ade33f/tblib-1.3.2.tar.gz"
            ],
            "strip_prefix": "tblib-1.3.2",
            "build_file": "//third_party:tblib.BUILD"
        },
        "repositories": [
            {
                "rule_class": "//third_party:repo.bzl%tf_http_archive",
                "attributes": {
                    "name": "tblib_archive",
                    "generator_name": "tblib_archive",
                    "generator_function": "tf_repositories",
                    "sha256": "436e4200e63d92316551179dc540906652878df4ff39b43db30fcf6400444fe7",
                    "urls": [
                        "http://mirror.tensorflow.org/files.pythonhosted.org/packages/ec/c4/8c651f3240a73c28a218194f3d527eb2be5a173d08501060cdee84ade33f/tblib-1.3.2.tar.gz",
                        "https://files.pythonhosted.org/packages/ec/c4/8c651f3240a73c28a218194f3d527eb2be5a173d08501060cdee84ade33f/tblib-1.3.2.tar.gz"
                    ],
                    "strip_prefix": "tblib-1.3.2",
                    "build_file": "//third_party:tblib.BUILD"
                },
                "output_tree_hash": "a0cb0bbbfdb564c8f89f9fb9672cce3654f9e4371734526abb627f97b018417e"
            }
        ]
    },
    {
        "original_rule_class": "//third_party:repo.bzl%tf_http_archive",
        "definition_information": "Repository rules_python instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule tf_http_archive defined at:\n  /home/abuild/rpmbuild/BUILD/tensorflow-2.3.1/third_party/repo.bzl:134:34: in <toplevel>\n",
        "original_attributes": {
            "name": "rules_python",
            "generator_name": "rules_python",
            "generator_function": "tf_repositories",
            "sha256": "aa96a691d3a8177f3215b14b0edc9641787abaaa30363a080165d06ab65e1161",
            "urls": [
                "https://storage.googleapis.com/mirror.tensorflow.org/github.com/bazelbuild/rules_python/releases/download/0.0.1/rules_python-0.0.1.tar.gz",
                "https://github.com/bazelbuild/rules_python/releases/download/0.0.1/rules_python-0.0.1.tar.gz"
            ]
        },
        "repositories": [
            {
                "rule_class": "//third_party:repo.bzl%tf_http_archive",
                "attributes": {
                    "name": "rules_python",
                    "generator_name": "rules_python",
                    "generator_function": "tf_repositories",
                    "sha256": "aa96a691d3a8177f3215b14b0edc9641787abaaa30363a080165d06ab65e1161",
                    "urls": [
                        "https://storage.googleapis.com/mirror.tensorflow.org/github.com/bazelbuild/rules_python/releases/download/0.0.1/rules_python-0.0.1.tar.gz",
                        "https://github.com/bazelbuild/rules_python/releases/download/0.0.1/rules_python-0.0.1.tar.gz"
                    ]
                },
                "output_tree_hash": "aaad5c22fdd4305c1696428871c54cef9569d24848a8e18e67d329aea68b8856"
            }
        ]
    },
    {
        "original_rule_class": "//third_party:repo.bzl%third_party_http_archive",
        "definition_information": "Repository clog instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule third_party_http_archive defined at:\n  /home/abuild/rpmbuild/BUILD/tensorflow-2.3.1/third_party/repo.bzl:219:43: in <toplevel>\n",
        "original_attributes": {
            "name": "clog",
            "generator_name": "clog",
            "generator_function": "tf_repositories",
            "sha256": "3f2dc1970f397a0e59db72f9fca6ff144b216895c1d606f6c94a507c1e53a025",
            "urls": [
                "https://storage.googleapis.com/mirror.tensorflow.org/github.com/pytorch/cpuinfo/archive/d5e37adf1406cf899d7d9ec1d317c47506ccb970.tar.gz",
                "https://github.com/pytorch/cpuinfo/archive/d5e37adf1406cf899d7d9ec1d317c47506ccb970.tar.gz"
            ],
            "strip_prefix": "cpuinfo-d5e37adf1406cf899d7d9ec1d317c47506ccb970",
            "build_file": "//third_party/clog:BUILD.bazel"
        },
        "repositories": [
            {
                "rule_class": "//third_party:repo.bzl%third_party_http_archive",
                "attributes": {
                    "name": "clog",
                    "generator_name": "clog",
                    "generator_function": "tf_repositories",
                    "sha256": "3f2dc1970f397a0e59db72f9fca6ff144b216895c1d606f6c94a507c1e53a025",
                    "urls": [
                        "https://storage.googleapis.com/mirror.tensorflow.org/github.com/pytorch/cpuinfo/archive/d5e37adf1406cf899d7d9ec1d317c47506ccb970.tar.gz",
                        "https://github.com/pytorch/cpuinfo/archive/d5e37adf1406cf899d7d9ec1d317c47506ccb970.tar.gz"
                    ],
                    "strip_prefix": "cpuinfo-d5e37adf1406cf899d7d9ec1d317c47506ccb970",
                    "build_file": "//third_party/clog:BUILD.bazel"
                },
                "output_tree_hash": "368421111f1f522c95731bab1d86aa9998777004c6c6ac08b3f794207717b22a"
            }
        ]
    },
    {
        "original_rule_class": "//third_party:repo.bzl%tf_http_archive",
        "definition_information": "Repository enum34_archive instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule tf_http_archive defined at:\n  /home/abuild/rpmbuild/BUILD/tensorflow-2.3.1/third_party/repo.bzl:134:34: in <toplevel>\n",
        "original_attributes": {
            "name": "enum34_archive",
            "generator_name": "enum34_archive",
            "generator_function": "tf_repositories",
            "sha256": "8ad8c4783bf61ded74527bffb48ed9b54166685e4230386a9ed9b1279e2df5b1",
            "urls": [
                "https://storage.googleapis.com/mirror.tensorflow.org/pypi.python.org/packages/bf/3e/31d502c25302814a7c2f1d3959d2a3b3f78e509002ba91aea64993936876/enum34-1.1.6.tar.gz",
                "https://pypi.python.org/packages/bf/3e/31d502c25302814a7c2f1d3959d2a3b3f78e509002ba91aea64993936876/enum34-1.1.6.tar.gz"
            ],
            "strip_prefix": "enum34-1.1.6/enum",
            "build_file": "//third_party:enum34.BUILD",
            "system_build_file": "//third_party/systemlibs:enum34.BUILD"
        },
        "repositories": [
            {
                "rule_class": "//third_party:repo.bzl%tf_http_archive",
                "attributes": {
                    "name": "enum34_archive",
                    "generator_name": "enum34_archive",
                    "generator_function": "tf_repositories",
                    "sha256": "8ad8c4783bf61ded74527bffb48ed9b54166685e4230386a9ed9b1279e2df5b1",
                    "urls": [
                        "https://storage.googleapis.com/mirror.tensorflow.org/pypi.python.org/packages/bf/3e/31d502c25302814a7c2f1d3959d2a3b3f78e509002ba91aea64993936876/enum34-1.1.6.tar.gz",
                        "https://pypi.python.org/packages/bf/3e/31d502c25302814a7c2f1d3959d2a3b3f78e509002ba91aea64993936876/enum34-1.1.6.tar.gz"
                    ],
                    "strip_prefix": "enum34-1.1.6/enum",
                    "build_file": "//third_party:enum34.BUILD",
                    "system_build_file": "//third_party/systemlibs:enum34.BUILD"
                },
                "output_tree_hash": "2c830a759e5d58f01a7b18ddbdee0f90189b7487dd32b5d011636ef2e1468d75"
            }
        ]
    },
    {
        "original_rule_class": "//third_party:repo.bzl%third_party_http_archive",
        "definition_information": "Repository highwayhash instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule third_party_http_archive defined at:\n  /home/abuild/rpmbuild/BUILD/tensorflow-2.3.1/third_party/repo.bzl:219:43: in <toplevel>\n",
        "original_attributes": {
            "name": "highwayhash",
            "generator_name": "highwayhash",
            "generator_function": "tf_repositories",
            "sha256": "9c3e0e87d581feeb0c18d814d98f170ff23e62967a2bd6855847f0b2fe598a37",
            "urls": [
                "https://storage.googleapis.com/mirror.tensorflow.org/github.com/google/highwayhash/archive/fd3d9af80465e4383162e4a7c5e2f406e82dd968.tar.gz",
                "https://github.com/google/highwayhash/archive/fd3d9af80465e4383162e4a7c5e2f406e82dd968.tar.gz"
            ],
            "strip_prefix": "highwayhash-fd3d9af80465e4383162e4a7c5e2f406e82dd968",
            "build_file": "//third_party/highwayhash:BUILD.bazel"
        },
        "repositories": [
            {
                "rule_class": "//third_party:repo.bzl%third_party_http_archive",
                "attributes": {
                    "name": "highwayhash",
                    "generator_name": "highwayhash",
                    "generator_function": "tf_repositories",
                    "sha256": "9c3e0e87d581feeb0c18d814d98f170ff23e62967a2bd6855847f0b2fe598a37",
                    "urls": [
                        "https://storage.googleapis.com/mirror.tensorflow.org/github.com/google/highwayhash/archive/fd3d9af80465e4383162e4a7c5e2f406e82dd968.tar.gz",
                        "https://github.com/google/highwayhash/archive/fd3d9af80465e4383162e4a7c5e2f406e82dd968.tar.gz"
                    ],
                    "strip_prefix": "highwayhash-fd3d9af80465e4383162e4a7c5e2f406e82dd968",
                    "build_file": "//third_party/highwayhash:BUILD.bazel"
                },
                "output_tree_hash": "33567a0c380b61ba55c55db8da64ecd627f2fd3af0cb71fd190468eba3e83011"
            }
        ]
    },
    {
        "original_rule_class": "//third_party/py:python_configure.bzl%local_python_configure",
        "definition_information": "Repository local_execution_config_python instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule local_python_configure defined at:\n  /home/abuild/rpmbuild/BUILD/tensorflow-2.3.1/third_party/py/python_configure.bzl:275:41: in <toplevel>\n",
        "original_attributes": {
            "name": "local_execution_config_python",
            "generator_name": "local_execution_config_python",
            "generator_function": "tf_repositories",
            "platform_constraint": "@local_execution_config_platform//:platform_constraint"
        },
        "repositories": [
            {
                "rule_class": "//third_party/py:python_configure.bzl%local_python_configure",
                "attributes": {
                    "name": "local_execution_config_python",
                    "generator_name": "local_execution_config_python",
                    "generator_function": "tf_repositories",
                    "platform_constraint": "@local_execution_config_platform//:platform_constraint"
                },
                "output_tree_hash": "ff9cc125018ebe5da185fe18fe80ff957ddbbdb2e7dbc4966337aa1995d41d7b"
            }
        ]
    },
    {
        "original_rule_class": "//third_party:repo.bzl%tf_http_archive",
        "definition_information": "Repository gif instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule tf_http_archive defined at:\n  /home/abuild/rpmbuild/BUILD/tensorflow-2.3.1/third_party/repo.bzl:134:34: in <toplevel>\n",
        "original_attributes": {
            "name": "gif",
            "generator_name": "gif",
            "generator_function": "tf_repositories",
            "sha256": "31da5562f44c5f15d63340a09a4fd62b48c45620cd302f77a6d9acf0077879bd",
            "urls": [
                "https://storage.googleapis.com/mirror.tensorflow.org/pilotfiber.dl.sourceforge.net/project/giflib/giflib-5.2.1.tar.gz",
                "https://pilotfiber.dl.sourceforge.net/project/giflib/giflib-5.2.1.tar.gz"
            ],
            "strip_prefix": "giflib-5.2.1",
            "patch_file": "//third_party:gif_fix_strtok_r.patch",
            "build_file": "//third_party:gif.BUILD",
            "system_build_file": "//third_party/systemlibs:gif.BUILD"
        },
        "repositories": [
            {
                "rule_class": "//third_party:repo.bzl%tf_http_archive",
                "attributes": {
                    "name": "gif",
                    "generator_name": "gif",
                    "generator_function": "tf_repositories",
                    "sha256": "31da5562f44c5f15d63340a09a4fd62b48c45620cd302f77a6d9acf0077879bd",
                    "urls": [
                        "https://storage.googleapis.com/mirror.tensorflow.org/pilotfiber.dl.sourceforge.net/project/giflib/giflib-5.2.1.tar.gz",
                        "https://pilotfiber.dl.sourceforge.net/project/giflib/giflib-5.2.1.tar.gz"
                    ],
                    "strip_prefix": "giflib-5.2.1",
                    "patch_file": "//third_party:gif_fix_strtok_r.patch",
                    "build_file": "//third_party:gif.BUILD",
                    "system_build_file": "//third_party/systemlibs:gif.BUILD"
                },
                "output_tree_hash": "3454f3ff83e16b13912cf0f286de59a872ae7d4cd6ba8f6065eadac4b0c81321"
            }
        ]
    },
    {
        "original_rule_class": "//third_party:repo.bzl%tf_http_archive",
        "definition_information": "Repository functools32_archive instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule tf_http_archive defined at:\n  /home/abuild/rpmbuild/BUILD/tensorflow-2.3.1/third_party/repo.bzl:134:34: in <toplevel>\n",
        "original_attributes": {
            "name": "functools32_archive",
            "generator_name": "functools32_archive",
            "generator_function": "tf_repositories",
            "sha256": "f6253dfbe0538ad2e387bd8fdfd9293c925d63553f5813c4e587745416501e6d",
            "urls": [
                "https://storage.googleapis.com/mirror.tensorflow.org/pypi.python.org/packages/c5/60/6ac26ad05857c601308d8fb9e87fa36d0ebf889423f47c3502ef034365db/functools32-3.2.3-2.tar.gz",
                "https://pypi.python.org/packages/c5/60/6ac26ad05857c601308d8fb9e87fa36d0ebf889423f47c3502ef034365db/functools32-3.2.3-2.tar.gz"
            ],
            "strip_prefix": "functools32-3.2.3-2",
            "build_file": "//third_party:functools32.BUILD",
            "system_build_file": "//third_party/systemlibs:functools32.BUILD"
        },
        "repositories": [
            {
                "rule_class": "//third_party:repo.bzl%tf_http_archive",
                "attributes": {
                    "name": "functools32_archive",
                    "generator_name": "functools32_archive",
                    "generator_function": "tf_repositories",
                    "sha256": "f6253dfbe0538ad2e387bd8fdfd9293c925d63553f5813c4e587745416501e6d",
                    "urls": [
                        "https://storage.googleapis.com/mirror.tensorflow.org/pypi.python.org/packages/c5/60/6ac26ad05857c601308d8fb9e87fa36d0ebf889423f47c3502ef034365db/functools32-3.2.3-2.tar.gz",
                        "https://pypi.python.org/packages/c5/60/6ac26ad05857c601308d8fb9e87fa36d0ebf889423f47c3502ef034365db/functools32-3.2.3-2.tar.gz"
                    ],
                    "strip_prefix": "functools32-3.2.3-2",
                    "build_file": "//third_party:functools32.BUILD",
                    "system_build_file": "//third_party/systemlibs:functools32.BUILD"
                },
                "output_tree_hash": "eb8e4899b1bec0cb7ead6fd50af2bc959512aebf7f35a79127a927b15d1cbb1c"
            }
        ]
    },
    {
        "original_rule_class": "//third_party:repo.bzl%tf_http_archive",
        "definition_information": "Repository lmdb instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule tf_http_archive defined at:\n  /home/abuild/rpmbuild/BUILD/tensorflow-2.3.1/third_party/repo.bzl:134:34: in <toplevel>\n",
        "original_attributes": {
            "name": "lmdb",
            "generator_name": "lmdb",
            "generator_function": "tf_repositories",
            "sha256": "f3927859882eb608868c8c31586bb7eb84562a40a6bf5cc3e13b6b564641ea28",
            "urls": [
                "https://storage.googleapis.com/mirror.tensorflow.org/github.com/LMDB/lmdb/archive/LMDB_0.9.22.tar.gz",
                "https://github.com/LMDB/lmdb/archive/LMDB_0.9.22.tar.gz"
            ],
            "strip_prefix": "lmdb-LMDB_0.9.22/libraries/liblmdb",
            "build_file": "//third_party:lmdb.BUILD",
            "system_build_file": "//third_party/systemlibs:lmdb.BUILD"
        },
        "repositories": [
            {
                "rule_class": "//third_party:repo.bzl%tf_http_archive",
                "attributes": {
                    "name": "lmdb",
                    "generator_name": "lmdb",
                    "generator_function": "tf_repositories",
                    "sha256": "f3927859882eb608868c8c31586bb7eb84562a40a6bf5cc3e13b6b564641ea28",
                    "urls": [
                        "https://storage.googleapis.com/mirror.tensorflow.org/github.com/LMDB/lmdb/archive/LMDB_0.9.22.tar.gz",
                        "https://github.com/LMDB/lmdb/archive/LMDB_0.9.22.tar.gz"
                    ],
                    "strip_prefix": "lmdb-LMDB_0.9.22/libraries/liblmdb",
                    "build_file": "//third_party:lmdb.BUILD",
                    "system_build_file": "//third_party/systemlibs:lmdb.BUILD"
                },
                "output_tree_hash": "e42f5b49d9be239811234e0b4e4ffa840967d8deadccb97940c83e48a2ca4dba"
            }
        ]
    },
    {
        "original_rule_class": "//third_party:repo.bzl%tf_http_archive",
        "definition_information": "Repository arm_neon_2_x86_sse instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule tf_http_archive defined at:\n  /home/abuild/rpmbuild/BUILD/tensorflow-2.3.1/third_party/repo.bzl:134:34: in <toplevel>\n",
        "original_attributes": {
            "name": "arm_neon_2_x86_sse",
            "generator_name": "arm_neon_2_x86_sse",
            "generator_function": "tf_repositories",
            "sha256": "213733991310b904b11b053ac224fee2d4e0179e46b52fe7f8735b8831e04dcc",
            "urls": [
                "https://storage.googleapis.com/mirror.tensorflow.org/github.com/intel/ARM_NEON_2_x86_SSE/archive/1200fe90bb174a6224a525ee60148671a786a71f.tar.gz",
                "https://github.com/intel/ARM_NEON_2_x86_SSE/archive/1200fe90bb174a6224a525ee60148671a786a71f.tar.gz"
            ],
            "strip_prefix": "ARM_NEON_2_x86_SSE-1200fe90bb174a6224a525ee60148671a786a71f",
            "build_file": "//third_party:arm_neon_2_x86_sse.BUILD"
        },
        "repositories": [
            {
                "rule_class": "//third_party:repo.bzl%tf_http_archive",
                "attributes": {
                    "name": "arm_neon_2_x86_sse",
                    "generator_name": "arm_neon_2_x86_sse",
                    "generator_function": "tf_repositories",
                    "sha256": "213733991310b904b11b053ac224fee2d4e0179e46b52fe7f8735b8831e04dcc",
                    "urls": [
                        "https://storage.googleapis.com/mirror.tensorflow.org/github.com/intel/ARM_NEON_2_x86_SSE/archive/1200fe90bb174a6224a525ee60148671a786a71f.tar.gz",
                        "https://github.com/intel/ARM_NEON_2_x86_SSE/archive/1200fe90bb174a6224a525ee60148671a786a71f.tar.gz"
                    ],
                    "strip_prefix": "ARM_NEON_2_x86_SSE-1200fe90bb174a6224a525ee60148671a786a71f",
                    "build_file": "//third_party:arm_neon_2_x86_sse.BUILD"
                },
                "output_tree_hash": "3e297e8d97838f86699a23b728da7464254d3539fbe9e6bd5ded7fe54c67888c"
            }
        ]
    },
    {
        "original_rule_class": "//third_party:repo.bzl%tf_http_archive",
        "definition_information": "Repository astor_archive instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule tf_http_archive defined at:\n  /home/abuild/rpmbuild/BUILD/tensorflow-2.3.1/third_party/repo.bzl:134:34: in <toplevel>\n",
        "original_attributes": {
            "name": "astor_archive",
            "generator_name": "astor_archive",
            "generator_function": "tf_repositories",
            "sha256": "95c30d87a6c2cf89aa628b87398466840f0ad8652f88eb173125a6df8533fb8d",
            "urls": [
                "https://storage.googleapis.com/mirror.tensorflow.org/pypi.python.org/packages/99/80/f9482277c919d28bebd85813c0a70117214149a96b08981b72b63240b84c/astor-0.7.1.tar.gz",
                "https://pypi.python.org/packages/99/80/f9482277c919d28bebd85813c0a70117214149a96b08981b72b63240b84c/astor-0.7.1.tar.gz"
            ],
            "strip_prefix": "astor-0.7.1",
            "build_file": "//third_party:astor.BUILD",
            "system_build_file": "//third_party/systemlibs:astor.BUILD"
        },
        "repositories": [
            {
                "rule_class": "//third_party:repo.bzl%tf_http_archive",
                "attributes": {
                    "name": "astor_archive",
                    "generator_name": "astor_archive",
                    "generator_function": "tf_repositories",
                    "sha256": "95c30d87a6c2cf89aa628b87398466840f0ad8652f88eb173125a6df8533fb8d",
                    "urls": [
                        "https://storage.googleapis.com/mirror.tensorflow.org/pypi.python.org/packages/99/80/f9482277c919d28bebd85813c0a70117214149a96b08981b72b63240b84c/astor-0.7.1.tar.gz",
                        "https://pypi.python.org/packages/99/80/f9482277c919d28bebd85813c0a70117214149a96b08981b72b63240b84c/astor-0.7.1.tar.gz"
                    ],
                    "strip_prefix": "astor-0.7.1",
                    "build_file": "//third_party:astor.BUILD",
                    "system_build_file": "//third_party/systemlibs:astor.BUILD"
                },
                "output_tree_hash": "fa4d3e1bdf6c8c5b24604ca8f916879f68559788c47930088daf0af125ad0f5c"
            }
        ]
    },
    {
        "original_rule_class": "//third_party:repo.bzl%tf_http_archive",
        "definition_information": "Repository opt_einsum_archive instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule tf_http_archive defined at:\n  /home/abuild/rpmbuild/BUILD/tensorflow-2.3.1/third_party/repo.bzl:134:34: in <toplevel>\n",
        "original_attributes": {
            "name": "opt_einsum_archive",
            "generator_name": "opt_einsum_archive",
            "generator_function": "tf_repositories",
            "sha256": "d3d464b4da7ef09e444c30e4003a27def37f85ff10ff2671e5f7d7813adac35b",
            "urls": [
                "https://storage.googleapis.com/mirror.tensorflow.org/pypi.python.org/packages/f6/d6/44792ec668bcda7d91913c75237314e688f70415ab2acd7172c845f0b24f/opt_einsum-2.3.2.tar.gz",
                "https://pypi.python.org/packages/f6/d6/44792ec668bcda7d91913c75237314e688f70415ab2acd7172c845f0b24f/opt_einsum-2.3.2.tar.gz"
            ],
            "strip_prefix": "opt_einsum-2.3.2",
            "build_file": "//third_party:opt_einsum.BUILD",
            "system_build_file": "//third_party/systemlibs:opt_einsum.BUILD"
        },
        "repositories": [
            {
                "rule_class": "//third_party:repo.bzl%tf_http_archive",
                "attributes": {
                    "name": "opt_einsum_archive",
                    "generator_name": "opt_einsum_archive",
                    "generator_function": "tf_repositories",
                    "sha256": "d3d464b4da7ef09e444c30e4003a27def37f85ff10ff2671e5f7d7813adac35b",
                    "urls": [
                        "https://storage.googleapis.com/mirror.tensorflow.org/pypi.python.org/packages/f6/d6/44792ec668bcda7d91913c75237314e688f70415ab2acd7172c845f0b24f/opt_einsum-2.3.2.tar.gz",
                        "https://pypi.python.org/packages/f6/d6/44792ec668bcda7d91913c75237314e688f70415ab2acd7172c845f0b24f/opt_einsum-2.3.2.tar.gz"
                    ],
                    "strip_prefix": "opt_einsum-2.3.2",
                    "build_file": "//third_party:opt_einsum.BUILD",
                    "system_build_file": "//third_party/systemlibs:opt_einsum.BUILD"
                },
                "output_tree_hash": "bb54a5ce08ea0a0a7adb512725e7ca7dbe128bf6f41271917dd0fcd54608192c"
            }
        ]
    },
    {
        "original_rule_class": "@bazel_tools//tools/cpp:cc_configure.bzl%cc_autoconf",
        "definition_information": "Repository local_config_cc instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule cc_autoconf defined at:\n  /home/abuild/rpmbuild/BUILD/output_user_root/55287d6e880b1ebc99135e2e30fc9816/external/bazel_tools/tools/cpp/cc_configure.bzl:143:30: in <toplevel>\n",
        "original_attributes": {
            "name": "local_config_cc",
            "generator_name": "local_config_cc",
            "generator_function": "cc_configure"
        },
        "repositories": [
            {
                "rule_class": "@bazel_tools//tools/cpp:cc_configure.bzl%cc_autoconf",
                "attributes": {
                    "name": "local_config_cc",
                    "generator_name": "local_config_cc",
                    "generator_function": "cc_configure"
                },
                "output_tree_hash": "484402f8091e97da0bbc591a4e3eee6a5ad18d584e55bdfd7a9cf0289954b77a"
            }
        ]
    },
    {
        "original_rule_class": "//third_party:repo.bzl%tf_http_archive",
        "definition_information": "Repository snappy instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule tf_http_archive defined at:\n  /home/abuild/rpmbuild/BUILD/tensorflow-2.3.1/third_party/repo.bzl:134:34: in <toplevel>\n",
        "original_attributes": {
            "name": "snappy",
            "generator_name": "snappy",
            "generator_function": "tf_repositories",
            "sha256": "16b677f07832a612b0836178db7f374e414f94657c138e6993cbfc5dcc58651f",
            "urls": [
                "https://storage.googleapis.com/mirror.tensorflow.org/github.com/google/snappy/archive/1.1.8.tar.gz",
                "https://github.com/google/snappy/archive/1.1.8.tar.gz"
            ],
            "strip_prefix": "snappy-1.1.8",
            "build_file": "//third_party:snappy.BUILD",
            "system_build_file": "//third_party/systemlibs:snappy.BUILD"
        },
        "repositories": [
            {
                "rule_class": "//third_party:repo.bzl%tf_http_archive",
                "attributes": {
                    "name": "snappy",
                    "generator_name": "snappy",
                    "generator_function": "tf_repositories",
                    "sha256": "16b677f07832a612b0836178db7f374e414f94657c138e6993cbfc5dcc58651f",
                    "urls": [
                        "https://storage.googleapis.com/mirror.tensorflow.org/github.com/google/snappy/archive/1.1.8.tar.gz",
                        "https://github.com/google/snappy/archive/1.1.8.tar.gz"
                    ],
                    "strip_prefix": "snappy-1.1.8",
                    "build_file": "//third_party:snappy.BUILD",
                    "system_build_file": "//third_party/systemlibs:snappy.BUILD"
                },
                "output_tree_hash": "c625089a15bd9f4c409f70a22656937f138206931d3b684fe56684e5b3ebd9fa"
            }
        ]
    },
    {
        "original_rule_class": "//third_party:repo.bzl%tf_http_archive",
        "definition_information": "Repository dill_archive instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule tf_http_archive defined at:\n  /home/abuild/rpmbuild/BUILD/tensorflow-2.3.1/third_party/repo.bzl:134:34: in <toplevel>\n",
        "original_attributes": {
            "name": "dill_archive",
            "generator_name": "dill_archive",
            "generator_function": "tf_repositories",
            "sha256": "42d8ef819367516592a825746a18073ced42ca169ab1f5f4044134703e7a049c",
            "urls": [
                "http://mirror.tensorflow.org/files.pythonhosted.org/packages/c7/11/345f3173809cea7f1a193bfbf02403fff250a3360e0e118a1630985e547d/dill-0.3.1.1.tar.gz",
                "https://files.pythonhosted.org/packages/c7/11/345f3173809cea7f1a193bfbf02403fff250a3360e0e118a1630985e547d/dill-0.3.1.1.tar.gz"
            ],
            "strip_prefix": "dill-0.3.1.1",
            "build_file": "//third_party:dill.BUILD"
        },
        "repositories": [
            {
                "rule_class": "//third_party:repo.bzl%tf_http_archive",
                "attributes": {
                    "name": "dill_archive",
                    "generator_name": "dill_archive",
                    "generator_function": "tf_repositories",
                    "sha256": "42d8ef819367516592a825746a18073ced42ca169ab1f5f4044134703e7a049c",
                    "urls": [
                        "http://mirror.tensorflow.org/files.pythonhosted.org/packages/c7/11/345f3173809cea7f1a193bfbf02403fff250a3360e0e118a1630985e547d/dill-0.3.1.1.tar.gz",
                        "https://files.pythonhosted.org/packages/c7/11/345f3173809cea7f1a193bfbf02403fff250a3360e0e118a1630985e547d/dill-0.3.1.1.tar.gz"
                    ],
                    "strip_prefix": "dill-0.3.1.1",
                    "build_file": "//third_party:dill.BUILD"
                },
                "output_tree_hash": "326d6afbc8a9345d0f973db46a494778afc2a1d32b417e8bdfd88c4a87be01d4"
            }
        ]
    },
    {
        "original_rule_class": "//third_party:repo.bzl%third_party_http_archive",
        "definition_information": "Repository libjpeg_turbo instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule third_party_http_archive defined at:\n  /home/abuild/rpmbuild/BUILD/tensorflow-2.3.1/third_party/repo.bzl:219:43: in <toplevel>\n",
        "original_attributes": {
            "name": "libjpeg_turbo",
            "generator_name": "libjpeg_turbo",
            "generator_function": "tf_repositories",
            "sha256": "7777c3c19762940cff42b3ba4d7cd5c52d1671b39a79532050c85efb99079064",
            "urls": [
                "https://storage.googleapis.com/mirror.tensorflow.org/github.com/libjpeg-turbo/libjpeg-turbo/archive/2.0.4.tar.gz",
                "https://github.com/libjpeg-turbo/libjpeg-turbo/archive/2.0.4.tar.gz"
            ],
            "strip_prefix": "libjpeg-turbo-2.0.4",
            "build_file": "//third_party/jpeg:BUILD.bazel",
            "system_build_file": "//third_party/jpeg:BUILD.system"
        },
        "repositories": [
            {
                "rule_class": "//third_party:repo.bzl%third_party_http_archive",
                "attributes": {
                    "name": "libjpeg_turbo",
                    "generator_name": "libjpeg_turbo",
                    "generator_function": "tf_repositories",
                    "sha256": "7777c3c19762940cff42b3ba4d7cd5c52d1671b39a79532050c85efb99079064",
                    "urls": [
                        "https://storage.googleapis.com/mirror.tensorflow.org/github.com/libjpeg-turbo/libjpeg-turbo/archive/2.0.4.tar.gz",
                        "https://github.com/libjpeg-turbo/libjpeg-turbo/archive/2.0.4.tar.gz"
                    ],
                    "strip_prefix": "libjpeg-turbo-2.0.4",
                    "build_file": "//third_party/jpeg:BUILD.bazel",
                    "system_build_file": "//third_party/jpeg:BUILD.system"
                },
                "output_tree_hash": "32b6fc58400128c0429aac8a267c77ca57c1be0b55c7540bd2c3f6f230c361d9"
            }
        ]
    },
    {
        "original_rule_class": "//third_party:repo.bzl%tf_http_archive",
        "definition_information": "Repository zlib instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule tf_http_archive defined at:\n  /home/abuild/rpmbuild/BUILD/tensorflow-2.3.1/third_party/repo.bzl:134:34: in <toplevel>\n",
        "original_attributes": {
            "name": "zlib",
            "generator_name": "zlib",
            "generator_function": "tf_repositories",
            "sha256": "c3e5e9fdd5004dcb542feda5ee4f0ff0744628baf8ed2dd5d66f8ca1197cb1a1",
            "urls": [
                "https://storage.googleapis.com/mirror.tensorflow.org/zlib.net/zlib-1.2.11.tar.gz",
                "https://zlib.net/zlib-1.2.11.tar.gz"
            ],
            "strip_prefix": "zlib-1.2.11",
            "build_file": "//third_party:zlib.BUILD",
            "system_build_file": "//third_party/systemlibs:zlib.BUILD"
        },
        "repositories": [
            {
                "rule_class": "//third_party:repo.bzl%tf_http_archive",
                "attributes": {
                    "name": "zlib",
                    "generator_name": "zlib",
                    "generator_function": "tf_repositories",
                    "sha256": "c3e5e9fdd5004dcb542feda5ee4f0ff0744628baf8ed2dd5d66f8ca1197cb1a1",
                    "urls": [
                        "https://storage.googleapis.com/mirror.tensorflow.org/zlib.net/zlib-1.2.11.tar.gz",
                        "https://zlib.net/zlib-1.2.11.tar.gz"
                    ],
                    "strip_prefix": "zlib-1.2.11",
                    "build_file": "//third_party:zlib.BUILD",
                    "system_build_file": "//third_party/systemlibs:zlib.BUILD"
                },
                "output_tree_hash": "e2e62cce10eb2d0044bc01741149c3e081caeacbbab47e531fe01ca868457816"
            }
        ]
    },
    {
        "original_rule_class": "//third_party:repo.bzl%tf_http_archive",
        "definition_information": "Repository com_google_absl instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule tf_http_archive defined at:\n  /home/abuild/rpmbuild/BUILD/tensorflow-2.3.1/third_party/repo.bzl:134:34: in <toplevel>\n",
        "original_attributes": {
            "name": "com_google_absl",
            "generator_name": "com_google_absl",
            "generator_function": "tf_repositories",
            "sha256": "f368a8476f4e2e0eccf8a7318b98dafbe30b2600f4e3cf52636e5eb145aba06a",
            "urls": [
                "https://storage.googleapis.com/mirror.tensorflow.org/github.com/abseil/abseil-cpp/archive/df3ea785d8c30a9503321a3d35ee7d35808f190d.tar.gz",
                "https://github.com/abseil/abseil-cpp/archive/df3ea785d8c30a9503321a3d35ee7d35808f190d.tar.gz"
            ],
            "strip_prefix": "abseil-cpp-df3ea785d8c30a9503321a3d35ee7d35808f190d",
            "patch_file": "//third_party:com_google_absl_fix_mac_and_nvcc_build.patch",
            "build_file": "//third_party:com_google_absl.BUILD"
        },
        "repositories": [
            {
                "rule_class": "//third_party:repo.bzl%tf_http_archive",
                "attributes": {
                    "name": "com_google_absl",
                    "generator_name": "com_google_absl",
                    "generator_function": "tf_repositories",
                    "sha256": "f368a8476f4e2e0eccf8a7318b98dafbe30b2600f4e3cf52636e5eb145aba06a",
                    "urls": [
                        "https://storage.googleapis.com/mirror.tensorflow.org/github.com/abseil/abseil-cpp/archive/df3ea785d8c30a9503321a3d35ee7d35808f190d.tar.gz",
                        "https://github.com/abseil/abseil-cpp/archive/df3ea785d8c30a9503321a3d35ee7d35808f190d.tar.gz"
                    ],
                    "strip_prefix": "abseil-cpp-df3ea785d8c30a9503321a3d35ee7d35808f190d",
                    "patch_file": "//third_party:com_google_absl_fix_mac_and_nvcc_build.patch",
                    "build_file": "//third_party:com_google_absl.BUILD"
                },
                "output_tree_hash": "eccafea67657b9b77f06e7f8ca2f0defcba11abe72bf35d190eb601ed1ca8c84"
            }
        ]
    },
    {
        "original_rule_class": "//third_party:repo.bzl%tf_http_archive",
        "definition_information": "Repository com_googlesource_code_re2 instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule tf_http_archive defined at:\n  /home/abuild/rpmbuild/BUILD/tensorflow-2.3.1/third_party/repo.bzl:134:34: in <toplevel>\n",
        "original_attributes": {
            "name": "com_googlesource_code_re2",
            "generator_name": "com_googlesource_code_re2",
            "generator_function": "tf_repositories",
            "sha256": "d070e2ffc5476c496a6a872a6f246bfddce8e7797d6ba605a7c8d72866743bf9",
            "urls": [
                "https://storage.googleapis.com/mirror.tensorflow.org/github.com/google/re2/archive/506cfa4bffd060c06ec338ce50ea3468daa6c814.tar.gz",
                "https://github.com/google/re2/archive/506cfa4bffd060c06ec338ce50ea3468daa6c814.tar.gz"
            ],
            "strip_prefix": "re2-506cfa4bffd060c06ec338ce50ea3468daa6c814",
            "system_build_file": "//third_party/systemlibs:re2.BUILD"
        },
        "repositories": [
            {
                "rule_class": "//third_party:repo.bzl%tf_http_archive",
                "attributes": {
                    "name": "com_googlesource_code_re2",
                    "generator_name": "com_googlesource_code_re2",
                    "generator_function": "tf_repositories",
                    "sha256": "d070e2ffc5476c496a6a872a6f246bfddce8e7797d6ba605a7c8d72866743bf9",
                    "urls": [
                        "https://storage.googleapis.com/mirror.tensorflow.org/github.com/google/re2/archive/506cfa4bffd060c06ec338ce50ea3468daa6c814.tar.gz",
                        "https://github.com/google/re2/archive/506cfa4bffd060c06ec338ce50ea3468daa6c814.tar.gz"
                    ],
                    "strip_prefix": "re2-506cfa4bffd060c06ec338ce50ea3468daa6c814",
                    "system_build_file": "//third_party/systemlibs:re2.BUILD"
                },
                "output_tree_hash": "fd6a56b00831e0b76bd0c0d9602b5598a3033cd4bea678a473b034a8b36e8902"
            }
        ]
    },
    {
        "original_rule_class": "//third_party:repo.bzl%tf_http_archive",
        "definition_information": "Repository pybind11 instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule tf_http_archive defined at:\n  /home/abuild/rpmbuild/BUILD/tensorflow-2.3.1/third_party/repo.bzl:134:34: in <toplevel>\n",
        "original_attributes": {
            "name": "pybind11",
            "generator_name": "pybind11",
            "generator_function": "tf_repositories",
            "sha256": "1eed57bc6863190e35637290f97a20c81cfe4d9090ac0a24f3bbf08f265eb71d",
            "urls": [
                "https://storage.googleapis.com/mirror.tensorflow.org/github.com/pybind/pybind11/archive/v2.4.3.tar.gz",
                "https://github.com/pybind/pybind11/archive/v2.4.3.tar.gz"
            ],
            "strip_prefix": "pybind11-2.4.3",
            "build_file": "//third_party:pybind11.BUILD",
            "system_build_file": "//third_party/systemlibs:pybind11.BUILD"
        },
        "repositories": [
            {
                "rule_class": "//third_party:repo.bzl%tf_http_archive",
                "attributes": {
                    "name": "pybind11",
                    "generator_name": "pybind11",
                    "generator_function": "tf_repositories",
                    "sha256": "1eed57bc6863190e35637290f97a20c81cfe4d9090ac0a24f3bbf08f265eb71d",
                    "urls": [
                        "https://storage.googleapis.com/mirror.tensorflow.org/github.com/pybind/pybind11/archive/v2.4.3.tar.gz",
                        "https://github.com/pybind/pybind11/archive/v2.4.3.tar.gz"
                    ],
                    "strip_prefix": "pybind11-2.4.3",
                    "build_file": "//third_party:pybind11.BUILD",
                    "system_build_file": "//third_party/systemlibs:pybind11.BUILD"
                },
                "output_tree_hash": "f6b8929bd20602077d6441af7f39dcf3cb0bbce27614bdc3cd3691ed67951c8c"
            }
        ]
    },
    {
        "original_rule_class": "//third_party:repo.bzl%third_party_http_archive",
        "definition_information": "Repository nasm instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule third_party_http_archive defined at:\n  /home/abuild/rpmbuild/BUILD/tensorflow-2.3.1/third_party/repo.bzl:219:43: in <toplevel>\n",
        "original_attributes": {
            "name": "nasm",
            "generator_name": "nasm",
            "generator_function": "tf_repositories",
            "sha256": "63ec86477ad3f0f6292325fd89e1d93aea2e2fd490070863f17d48f7cd387011",
            "urls": [
                "https://storage.googleapis.com/mirror.tensorflow.org/www.nasm.us/pub/nasm/releasebuilds/2.13.03/nasm-2.13.03.tar.bz2",
                "http://pkgs.fedoraproject.org/repo/pkgs/nasm/nasm-2.13.03.tar.bz2/sha512/d7a6b4cee8dfd603d8d4c976e5287b5cc542fa0b466ff989b743276a6e28114e64289bf02a7819eca63142a5278aa6eed57773007e5f589e15768e6456a8919d/nasm-2.13.03.tar.bz2",
                "http://www.nasm.us/pub/nasm/releasebuilds/2.13.03/nasm-2.13.03.tar.bz2"
            ],
            "strip_prefix": "nasm-2.13.03",
            "build_file": "//third_party/nasm:BUILD.bazel",
            "system_build_file": "//third_party/nasm:BUILD.system"
        },
        "repositories": [
            {
                "rule_class": "//third_party:repo.bzl%third_party_http_archive",
                "attributes": {
                    "name": "nasm",
                    "generator_name": "nasm",
                    "generator_function": "tf_repositories",
                    "sha256": "63ec86477ad3f0f6292325fd89e1d93aea2e2fd490070863f17d48f7cd387011",
                    "urls": [
                        "https://storage.googleapis.com/mirror.tensorflow.org/www.nasm.us/pub/nasm/releasebuilds/2.13.03/nasm-2.13.03.tar.bz2",
                        "http://pkgs.fedoraproject.org/repo/pkgs/nasm/nasm-2.13.03.tar.bz2/sha512/d7a6b4cee8dfd603d8d4c976e5287b5cc542fa0b466ff989b743276a6e28114e64289bf02a7819eca63142a5278aa6eed57773007e5f589e15768e6456a8919d/nasm-2.13.03.tar.bz2",
                        "http://www.nasm.us/pub/nasm/releasebuilds/2.13.03/nasm-2.13.03.tar.bz2"
                    ],
                    "strip_prefix": "nasm-2.13.03",
                    "build_file": "//third_party/nasm:BUILD.bazel",
                    "system_build_file": "//third_party/nasm:BUILD.system"
                },
                "output_tree_hash": "85f7c31320dc50f64e44ba087197b3af3ea96f0df25342690ee604b5157223af"
            }
        ]
    },
    {
        "original_rule_class": "//third_party:repo.bzl%tf_http_archive",
        "definition_information": "Repository curl instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule tf_http_archive defined at:\n  /home/abuild/rpmbuild/BUILD/tensorflow-2.3.1/third_party/repo.bzl:134:34: in <toplevel>\n",
        "original_attributes": {
            "name": "curl",
            "generator_name": "curl",
            "generator_function": "tf_repositories",
            "sha256": "01ae0c123dee45b01bbaef94c0bc00ed2aec89cb2ee0fd598e0d302a6b5e0a98",
            "urls": [
                "https://storage.googleapis.com/mirror.tensorflow.org/curl.haxx.se/download/curl-7.69.1.tar.gz",
                "https://curl.haxx.se/download/curl-7.69.1.tar.gz"
            ],
            "strip_prefix": "curl-7.69.1",
            "build_file": "//third_party:curl.BUILD",
            "system_build_file": "//third_party/systemlibs:curl.BUILD"
        },
        "repositories": [
            {
                "rule_class": "//third_party:repo.bzl%tf_http_archive",
                "attributes": {
                    "name": "curl",
                    "generator_name": "curl",
                    "generator_function": "tf_repositories",
                    "sha256": "01ae0c123dee45b01bbaef94c0bc00ed2aec89cb2ee0fd598e0d302a6b5e0a98",
                    "urls": [
                        "https://storage.googleapis.com/mirror.tensorflow.org/curl.haxx.se/download/curl-7.69.1.tar.gz",
                        "https://curl.haxx.se/download/curl-7.69.1.tar.gz"
                    ],
                    "strip_prefix": "curl-7.69.1",
                    "build_file": "//third_party:curl.BUILD",
                    "system_build_file": "//third_party/systemlibs:curl.BUILD"
                },
                "output_tree_hash": "c6a1d9a53308b931f86fb7be0553b273409b4600db1b688c390b599ce45f70c3"
            }
        ]
    },
    {
        "original_rule_class": "//third_party/git:git_configure.bzl%git_configure",
        "definition_information": "Repository local_config_git instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule git_configure defined at:\n  /home/abuild/rpmbuild/BUILD/tensorflow-2.3.1/third_party/git/git_configure.bzl:66:32: in <toplevel>\n",
        "original_attributes": {
            "name": "local_config_git",
            "generator_name": "local_config_git",
            "generator_function": "tf_repositories"
        },
        "repositories": [
            {
                "rule_class": "//third_party/git:git_configure.bzl%git_configure",
                "attributes": {
                    "name": "local_config_git",
                    "generator_name": "local_config_git",
                    "generator_function": "tf_repositories"
                },
                "output_tree_hash": "a11b2aab9702bf4719a039ca02fc5930e57bb139270076a6522341ba89f7922e"
            }
        ]
    },
    {
        "original_rule_class": "//third_party:repo.bzl%tf_http_archive",
        "definition_information": "Repository png instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule tf_http_archive defined at:\n  /home/abuild/rpmbuild/BUILD/tensorflow-2.3.1/third_party/repo.bzl:134:34: in <toplevel>\n",
        "original_attributes": {
            "name": "png",
            "generator_name": "png",
            "generator_function": "tf_repositories",
            "sha256": "ca74a0dace179a8422187671aee97dd3892b53e168627145271cad5b5ac81307",
            "urls": [
                "https://storage.googleapis.com/mirror.tensorflow.org/github.com/glennrp/libpng/archive/v1.6.37.tar.gz",
                "https://github.com/glennrp/libpng/archive/v1.6.37.tar.gz"
            ],
            "strip_prefix": "libpng-1.6.37",
            "patch_file": "//third_party:png_fix_rpi.patch",
            "build_file": "//third_party:png.BUILD",
            "system_build_file": "//third_party/systemlibs:png.BUILD"
        },
        "repositories": [
            {
                "rule_class": "//third_party:repo.bzl%tf_http_archive",
                "attributes": {
                    "name": "png",
                    "generator_name": "png",
                    "generator_function": "tf_repositories",
                    "sha256": "ca74a0dace179a8422187671aee97dd3892b53e168627145271cad5b5ac81307",
                    "urls": [
                        "https://storage.googleapis.com/mirror.tensorflow.org/github.com/glennrp/libpng/archive/v1.6.37.tar.gz",
                        "https://github.com/glennrp/libpng/archive/v1.6.37.tar.gz"
                    ],
                    "strip_prefix": "libpng-1.6.37",
                    "patch_file": "//third_party:png_fix_rpi.patch",
                    "build_file": "//third_party:png.BUILD",
                    "system_build_file": "//third_party/systemlibs:png.BUILD"
                },
                "output_tree_hash": "44e1ec3a71669b4c0676a19026d6bbcea4c1171045fe5e7c9a7d51b243dfea31"
            }
        ]
    },
    {
        "original_rule_class": "//third_party:repo.bzl%tf_http_archive",
        "definition_information": "Repository cython instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule tf_http_archive defined at:\n  /home/abuild/rpmbuild/BUILD/tensorflow-2.3.1/third_party/repo.bzl:134:34: in <toplevel>\n",
        "original_attributes": {
            "name": "cython",
            "generator_name": "cython",
            "generator_function": "tf_repositories",
            "sha256": "bccc9aa050ea02595b2440188813b936eaf345e85fb9692790cecfe095cf91aa",
            "urls": [
                "https://storage.googleapis.com/mirror.tensorflow.org/github.com/cython/cython/archive/0.28.4.tar.gz",
                "https://github.com/cython/cython/archive/0.28.4.tar.gz"
            ],
            "strip_prefix": "cython-0.28.4",
            "delete": [
                "BUILD.bazel"
            ],
            "build_file": "//third_party:cython.BUILD",
            "system_build_file": "//third_party/systemlibs:cython.BUILD"
        },
        "repositories": [
            {
                "rule_class": "//third_party:repo.bzl%tf_http_archive",
                "attributes": {
                    "name": "cython",
                    "generator_name": "cython",
                    "generator_function": "tf_repositories",
                    "sha256": "bccc9aa050ea02595b2440188813b936eaf345e85fb9692790cecfe095cf91aa",
                    "urls": [
                        "https://storage.googleapis.com/mirror.tensorflow.org/github.com/cython/cython/archive/0.28.4.tar.gz",
                        "https://github.com/cython/cython/archive/0.28.4.tar.gz"
                    ],
                    "strip_prefix": "cython-0.28.4",
                    "delete": [
                        "BUILD.bazel"
                    ],
                    "build_file": "//third_party:cython.BUILD",
                    "system_build_file": "//third_party/systemlibs:cython.BUILD"
                },
                "output_tree_hash": "2b234734405566737737eb2050b0af75c43d8c798fdb986c58815769b5f8e399"
            }
        ]
    },
    {
        "original_rule_class": "//third_party:repo.bzl%tf_http_archive",
        "definition_information": "Repository eigen_archive instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule tf_http_archive defined at:\n  /home/abuild/rpmbuild/BUILD/tensorflow-2.3.1/third_party/repo.bzl:134:34: in <toplevel>\n",
        "original_attributes": {
            "name": "eigen_archive",
            "generator_name": "eigen_archive",
            "generator_function": "tf_repositories",
            "sha256": "f632d82e43ffc46adfac9043beace700b0265748075e7edc0701d81380258038",
            "urls": [
                "https://storage.googleapis.com/mirror.tensorflow.org/gitlab.com/libeigen/eigen/-/archive/386d809bde475c65b7940f290efe80e6a05878c4/eigen-386d809bde475c65b7940f290efe80e6a05878c4.tar.gz",
                "https://gitlab.com/libeigen/eigen/-/archive/386d809bde475c65b7940f290efe80e6a05878c4/eigen-386d809bde475c65b7940f290efe80e6a05878c4.tar.gz"
            ],
            "strip_prefix": "eigen-386d809bde475c65b7940f290efe80e6a05878c4",
            "patch_file": "//third_party/eigen3:gpu_packet_math.patch",
            "build_file": "//third_party:eigen.BUILD"
        },
        "repositories": [
            {
                "rule_class": "//third_party:repo.bzl%tf_http_archive",
                "attributes": {
                    "name": "eigen_archive",
                    "generator_name": "eigen_archive",
                    "generator_function": "tf_repositories",
                    "sha256": "f632d82e43ffc46adfac9043beace700b0265748075e7edc0701d81380258038",
                    "urls": [
                        "https://storage.googleapis.com/mirror.tensorflow.org/gitlab.com/libeigen/eigen/-/archive/386d809bde475c65b7940f290efe80e6a05878c4/eigen-386d809bde475c65b7940f290efe80e6a05878c4.tar.gz",
                        "https://gitlab.com/libeigen/eigen/-/archive/386d809bde475c65b7940f290efe80e6a05878c4/eigen-386d809bde475c65b7940f290efe80e6a05878c4.tar.gz"
                    ],
                    "strip_prefix": "eigen-386d809bde475c65b7940f290efe80e6a05878c4",
                    "patch_file": "//third_party/eigen3:gpu_packet_math.patch",
                    "build_file": "//third_party:eigen.BUILD"
                },
                "output_tree_hash": "6951c4f8853551bc326f80b956911fd4033c3bf214809cd107c9629b2635a3bb"
            }
        ]
    },
    {
        "original_rule_class": "//third_party:repo.bzl%tf_http_archive",
        "definition_information": "Repository nsync instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule tf_http_archive defined at:\n  /home/abuild/rpmbuild/BUILD/tensorflow-2.3.1/third_party/repo.bzl:134:34: in <toplevel>\n",
        "original_attributes": {
            "name": "nsync",
            "generator_name": "nsync",
            "generator_function": "tf_repositories",
            "sha256": "caf32e6b3d478b78cff6c2ba009c3400f8251f646804bcb65465666a9cea93c4",
            "urls": [
                "https://storage.googleapis.com/mirror.tensorflow.org/github.com/google/nsync/archive/1.22.0.tar.gz",
                "https://github.com/google/nsync/archive/1.22.0.tar.gz"
            ],
            "strip_prefix": "nsync-1.22.0",
            "system_build_file": "//third_party/systemlibs:nsync.BUILD"
        },
        "repositories": [
            {
                "rule_class": "//third_party:repo.bzl%tf_http_archive",
                "attributes": {
                    "name": "nsync",
                    "generator_name": "nsync",
                    "generator_function": "tf_repositories",
                    "sha256": "caf32e6b3d478b78cff6c2ba009c3400f8251f646804bcb65465666a9cea93c4",
                    "urls": [
                        "https://storage.googleapis.com/mirror.tensorflow.org/github.com/google/nsync/archive/1.22.0.tar.gz",
                        "https://github.com/google/nsync/archive/1.22.0.tar.gz"
                    ],
                    "strip_prefix": "nsync-1.22.0",
                    "system_build_file": "//third_party/systemlibs:nsync.BUILD"
                },
                "output_tree_hash": "0cd0ca4b73f75b5826bf679ecf80cea86382dde7e1c6fdac9796e789e7afa679"
            }
        ]
    },
    {
        "original_rule_class": "//third_party:repo.bzl%tf_http_archive",
        "definition_information": "Repository jsoncpp_git instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule tf_http_archive defined at:\n  /home/abuild/rpmbuild/BUILD/tensorflow-2.3.1/third_party/repo.bzl:134:34: in <toplevel>\n",
        "original_attributes": {
            "name": "jsoncpp_git",
            "generator_name": "jsoncpp_git",
            "generator_function": "tf_repositories",
            "sha256": "77a402fb577b2e0e5d0bdc1cf9c65278915cdb25171e3452c68b6da8a561f8f0",
            "urls": [
                "https://storage.googleapis.com/mirror.tensorflow.org/github.com/open-source-parsers/jsoncpp/archive/1.9.2.tar.gz",
                "https://github.com/open-source-parsers/jsoncpp/archive/1.9.2.tar.gz"
            ],
            "strip_prefix": "jsoncpp-1.9.2",
            "build_file": "//third_party:jsoncpp.BUILD",
            "system_build_file": "//third_party/systemlibs:jsoncpp.BUILD"
        },
        "repositories": [
            {
                "rule_class": "//third_party:repo.bzl%tf_http_archive",
                "attributes": {
                    "name": "jsoncpp_git",
                    "generator_name": "jsoncpp_git",
                    "generator_function": "tf_repositories",
                    "sha256": "77a402fb577b2e0e5d0bdc1cf9c65278915cdb25171e3452c68b6da8a561f8f0",
                    "urls": [
                        "https://storage.googleapis.com/mirror.tensorflow.org/github.com/open-source-parsers/jsoncpp/archive/1.9.2.tar.gz",
                        "https://github.com/open-source-parsers/jsoncpp/archive/1.9.2.tar.gz"
                    ],
                    "strip_prefix": "jsoncpp-1.9.2",
                    "build_file": "//third_party:jsoncpp.BUILD",
                    "system_build_file": "//third_party/systemlibs:jsoncpp.BUILD"
                },
                "output_tree_hash": "9ce12a40acf8abdfad0b484d7e015ab010f374eeb689f0ba6ae60a1c9f4f3046"
            }
        ]
    },
    {
        "original_rule_class": "//third_party:repo.bzl%tf_http_archive",
        "definition_information": "Repository gemmlowp instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule tf_http_archive defined at:\n  /home/abuild/rpmbuild/BUILD/tensorflow-2.3.1/third_party/repo.bzl:134:34: in <toplevel>\n",
        "original_attributes": {
            "name": "gemmlowp",
            "generator_name": "gemmlowp",
            "generator_function": "tf_repositories",
            "sha256": "43146e6f56cb5218a8caaab6b5d1601a083f1f31c06ff474a4378a7d35be9cfb",
            "urls": [
                "https://storage.googleapis.com/mirror.tensorflow.org/github.com/google/gemmlowp/archive/fda83bdc38b118cc6b56753bd540caa49e570745.zip",
                "https://github.com/google/gemmlowp/archive/fda83bdc38b118cc6b56753bd540caa49e570745.zip"
            ],
            "strip_prefix": "gemmlowp-fda83bdc38b118cc6b56753bd540caa49e570745"
        },
        "repositories": [
            {
                "rule_class": "//third_party:repo.bzl%tf_http_archive",
                "attributes": {
                    "name": "gemmlowp",
                    "generator_name": "gemmlowp",
                    "generator_function": "tf_repositories",
                    "sha256": "43146e6f56cb5218a8caaab6b5d1601a083f1f31c06ff474a4378a7d35be9cfb",
                    "urls": [
                        "https://storage.googleapis.com/mirror.tensorflow.org/github.com/google/gemmlowp/archive/fda83bdc38b118cc6b56753bd540caa49e570745.zip",
                        "https://github.com/google/gemmlowp/archive/fda83bdc38b118cc6b56753bd540caa49e570745.zip"
                    ],
                    "strip_prefix": "gemmlowp-fda83bdc38b118cc6b56753bd540caa49e570745"
                },
                "output_tree_hash": "7249a2152217d0ed480cccb84153d188d6a92906b94291fcfc6733aaef54cdf4"
            }
        ]
    },
    {
        "original_rule_class": "//third_party:repo.bzl%tf_http_archive",
        "definition_information": "Repository termcolor_archive instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule tf_http_archive defined at:\n  /home/abuild/rpmbuild/BUILD/tensorflow-2.3.1/third_party/repo.bzl:134:34: in <toplevel>\n",
        "original_attributes": {
            "name": "termcolor_archive",
            "generator_name": "termcolor_archive",
            "generator_function": "tf_repositories",
            "sha256": "1d6d69ce66211143803fbc56652b41d73b4a400a2891d7bf7a1cdf4c02de613b",
            "urls": [
                "https://storage.googleapis.com/mirror.tensorflow.org/pypi.python.org/packages/8a/48/a76be51647d0eb9f10e2a4511bf3ffb8cc1e6b14e9e4fab46173aa79f981/termcolor-1.1.0.tar.gz",
                "https://pypi.python.org/packages/8a/48/a76be51647d0eb9f10e2a4511bf3ffb8cc1e6b14e9e4fab46173aa79f981/termcolor-1.1.0.tar.gz"
            ],
            "strip_prefix": "termcolor-1.1.0",
            "build_file": "//third_party:termcolor.BUILD",
            "system_build_file": "//third_party/systemlibs:termcolor.BUILD"
        },
        "repositories": [
            {
                "rule_class": "//third_party:repo.bzl%tf_http_archive",
                "attributes": {
                    "name": "termcolor_archive",
                    "generator_name": "termcolor_archive",
                    "generator_function": "tf_repositories",
                    "sha256": "1d6d69ce66211143803fbc56652b41d73b4a400a2891d7bf7a1cdf4c02de613b",
                    "urls": [
                        "https://storage.googleapis.com/mirror.tensorflow.org/pypi.python.org/packages/8a/48/a76be51647d0eb9f10e2a4511bf3ffb8cc1e6b14e9e4fab46173aa79f981/termcolor-1.1.0.tar.gz",
                        "https://pypi.python.org/packages/8a/48/a76be51647d0eb9f10e2a4511bf3ffb8cc1e6b14e9e4fab46173aa79f981/termcolor-1.1.0.tar.gz"
                    ],
                    "strip_prefix": "termcolor-1.1.0",
                    "build_file": "//third_party:termcolor.BUILD",
                    "system_build_file": "//third_party/systemlibs:termcolor.BUILD"
                },
                "output_tree_hash": "f00a46980752eb4e318f12c0a4533c1140843bc470cba450e794c7596578ade5"
            }
        ]
    },
    {
        "original_rule_class": "//third_party:repo.bzl%tf_http_archive",
        "definition_information": "Repository com_github_googlecloudplatform_google_cloud_cpp instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule tf_http_archive defined at:\n  /home/abuild/rpmbuild/BUILD/tensorflow-2.3.1/third_party/repo.bzl:134:34: in <toplevel>\n",
        "original_attributes": {
            "name": "com_github_googlecloudplatform_google_cloud_cpp",
            "generator_name": "com_github_googlecloudplatform_google_cloud_cpp",
            "generator_function": "tf_repositories",
            "sha256": "839b2d4dcb36a671734dac6b30ea8c298bbeaafcf7a45ee4a7d7aa5986b16569",
            "urls": [
                "https://storage.googleapis.com/mirror.tensorflow.org/github.com/googleapis/google-cloud-cpp/archive/v1.14.0.tar.gz",
                "https://github.com/googleapis/google-cloud-cpp/archive/v1.14.0.tar.gz"
            ],
            "strip_prefix": "google-cloud-cpp-1.14.0",
            "system_build_file": "//third_party/systemlibs:google_cloud_cpp.BUILD",
            "system_link_files": {
                "//third_party/systemlibs:google_cloud_cpp.google.cloud.bigtable.BUILD": "google/cloud/bigtable/BUILD"
            }
        },
        "repositories": [
            {
                "rule_class": "//third_party:repo.bzl%tf_http_archive",
                "attributes": {
                    "name": "com_github_googlecloudplatform_google_cloud_cpp",
                    "generator_name": "com_github_googlecloudplatform_google_cloud_cpp",
                    "generator_function": "tf_repositories",
                    "sha256": "839b2d4dcb36a671734dac6b30ea8c298bbeaafcf7a45ee4a7d7aa5986b16569",
                    "urls": [
                        "https://storage.googleapis.com/mirror.tensorflow.org/github.com/googleapis/google-cloud-cpp/archive/v1.14.0.tar.gz",
                        "https://github.com/googleapis/google-cloud-cpp/archive/v1.14.0.tar.gz"
                    ],
                    "strip_prefix": "google-cloud-cpp-1.14.0",
                    "system_build_file": "//third_party/systemlibs:google_cloud_cpp.BUILD",
                    "system_link_files": {
                        "//third_party/systemlibs:google_cloud_cpp.google.cloud.bigtable.BUILD": "google/cloud/bigtable/BUILD"
                    }
                },
                "output_tree_hash": "24294f38c948d44fa305c746809c54f20f176edecfe96ab1debaf01ae093b980"
            }
        ]
    },
    {
        "original_rule_class": "//third_party:repo.bzl%tf_http_archive",
        "definition_information": "Repository double_conversion instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule tf_http_archive defined at:\n  /home/abuild/rpmbuild/BUILD/tensorflow-2.3.1/third_party/repo.bzl:134:34: in <toplevel>\n",
        "original_attributes": {
            "name": "double_conversion",
            "generator_name": "double_conversion",
            "generator_function": "tf_repositories",
            "sha256": "2f7fbffac0d98d201ad0586f686034371a6d152ca67508ab611adc2386ad30de",
            "urls": [
                "https://storage.googleapis.com/mirror.tensorflow.org/github.com/google/double-conversion/archive/3992066a95b823efc8ccc1baf82a1cfc73f6e9b8.zip",
                "https://github.com/google/double-conversion/archive/3992066a95b823efc8ccc1baf82a1cfc73f6e9b8.zip"
            ],
            "strip_prefix": "double-conversion-3992066a95b823efc8ccc1baf82a1cfc73f6e9b8",
            "build_file": "//third_party:double_conversion.BUILD",
            "system_build_file": "//third_party/systemlibs:double_conversion.BUILD"
        },
        "repositories": [
            {
                "rule_class": "//third_party:repo.bzl%tf_http_archive",
                "attributes": {
                    "name": "double_conversion",
                    "generator_name": "double_conversion",
                    "generator_function": "tf_repositories",
                    "sha256": "2f7fbffac0d98d201ad0586f686034371a6d152ca67508ab611adc2386ad30de",
                    "urls": [
                        "https://storage.googleapis.com/mirror.tensorflow.org/github.com/google/double-conversion/archive/3992066a95b823efc8ccc1baf82a1cfc73f6e9b8.zip",
                        "https://github.com/google/double-conversion/archive/3992066a95b823efc8ccc1baf82a1cfc73f6e9b8.zip"
                    ],
                    "strip_prefix": "double-conversion-3992066a95b823efc8ccc1baf82a1cfc73f6e9b8",
                    "build_file": "//third_party:double_conversion.BUILD",
                    "system_build_file": "//third_party/systemlibs:double_conversion.BUILD"
                },
                "output_tree_hash": "20dfa5fd9077b65fe2be2b436fb46058592856aa6588bf5cf2ad68157fed2c0d"
            }
        ]
    },
    {
        "original_rule_class": "//third_party:repo.bzl%tf_http_archive",
        "definition_information": "Repository org_python_pypi_backports_weakref instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule tf_http_archive defined at:\n  /home/abuild/rpmbuild/BUILD/tensorflow-2.3.1/third_party/repo.bzl:134:34: in <toplevel>\n",
        "original_attributes": {
            "name": "org_python_pypi_backports_weakref",
            "generator_name": "org_python_pypi_backports_weakref",
            "generator_function": "tf_repositories",
            "sha256": "8813bf712a66b3d8b85dc289e1104ed220f1878cf981e2fe756dfaabe9a82892",
            "urls": [
                "https://storage.googleapis.com/mirror.tensorflow.org/pypi.python.org/packages/bc/cc/3cdb0a02e7e96f6c70bd971bc8a90b8463fda83e264fa9c5c1c98ceabd81/backports.weakref-1.0rc1.tar.gz",
                "https://pypi.python.org/packages/bc/cc/3cdb0a02e7e96f6c70bd971bc8a90b8463fda83e264fa9c5c1c98ceabd81/backports.weakref-1.0rc1.tar.gz"
            ],
            "strip_prefix": "backports.weakref-1.0rc1/src",
            "build_file": "//third_party:backports_weakref.BUILD"
        },
        "repositories": [
            {
                "rule_class": "//third_party:repo.bzl%tf_http_archive",
                "attributes": {
                    "name": "org_python_pypi_backports_weakref",
                    "generator_name": "org_python_pypi_backports_weakref",
                    "generator_function": "tf_repositories",
                    "sha256": "8813bf712a66b3d8b85dc289e1104ed220f1878cf981e2fe756dfaabe9a82892",
                    "urls": [
                        "https://storage.googleapis.com/mirror.tensorflow.org/pypi.python.org/packages/bc/cc/3cdb0a02e7e96f6c70bd971bc8a90b8463fda83e264fa9c5c1c98ceabd81/backports.weakref-1.0rc1.tar.gz",
                        "https://pypi.python.org/packages/bc/cc/3cdb0a02e7e96f6c70bd971bc8a90b8463fda83e264fa9c5c1c98ceabd81/backports.weakref-1.0rc1.tar.gz"
                    ],
                    "strip_prefix": "backports.weakref-1.0rc1/src",
                    "build_file": "//third_party:backports_weakref.BUILD"
                },
                "output_tree_hash": "caf2c3a15008539ec95dc6031edd158d63cfbae4ca3768adbc12676bdc53bffb"
            }
        ]
    },
    {
        "original_rule_class": "//third_party:repo.bzl%third_party_http_archive",
        "definition_information": "Repository dlpack instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule third_party_http_archive defined at:\n  /home/abuild/rpmbuild/BUILD/tensorflow-2.3.1/third_party/repo.bzl:219:43: in <toplevel>\n",
        "original_attributes": {
            "name": "dlpack",
            "generator_name": "dlpack",
            "generator_function": "tf_repositories",
            "sha256": "b59586ce69bcf3efdbf3cf4803fadfeaae4948044e2b8d89cf912194cf28f233",
            "urls": [
                "https://storage.googleapis.com/mirror.tensorflow.org/github.com/dmlc/dlpack/archive/3efc489b55385936531a06ff83425b719387ec63.tar.gz",
                "https://github.com/dmlc/dlpack/archive/3efc489b55385936531a06ff83425b719387ec63.tar.gz"
            ],
            "strip_prefix": "dlpack-3efc489b55385936531a06ff83425b719387ec63",
            "build_file": "//third_party/dlpack:BUILD.bazel"
        },
        "repositories": [
            {
                "rule_class": "//third_party:repo.bzl%third_party_http_archive",
                "attributes": {
                    "name": "dlpack",
                    "generator_name": "dlpack",
                    "generator_function": "tf_repositories",
                    "sha256": "b59586ce69bcf3efdbf3cf4803fadfeaae4948044e2b8d89cf912194cf28f233",
                    "urls": [
                        "https://storage.googleapis.com/mirror.tensorflow.org/github.com/dmlc/dlpack/archive/3efc489b55385936531a06ff83425b719387ec63.tar.gz",
                        "https://github.com/dmlc/dlpack/archive/3efc489b55385936531a06ff83425b719387ec63.tar.gz"
                    ],
                    "strip_prefix": "dlpack-3efc489b55385936531a06ff83425b719387ec63",
                    "build_file": "//third_party/dlpack:BUILD.bazel"
                },
                "output_tree_hash": "7da8aef554e3d455210f4b0dc9f53eb62c8adf3711e698249b7a9615ecbbcd50"
            }
        ]
    },
    {
        "original_rule_class": "//third_party:repo.bzl%tf_http_archive",
        "definition_information": "Repository farmhash_archive instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule tf_http_archive defined at:\n  /home/abuild/rpmbuild/BUILD/tensorflow-2.3.1/third_party/repo.bzl:134:34: in <toplevel>\n",
        "original_attributes": {
            "name": "farmhash_archive",
            "generator_name": "farmhash_archive",
            "generator_function": "tf_repositories",
            "sha256": "6560547c63e4af82b0f202cb710ceabb3f21347a4b996db565a411da5b17aba0",
            "urls": [
                "https://storage.googleapis.com/mirror.tensorflow.org/github.com/google/farmhash/archive/816a4ae622e964763ca0862d9dbd19324a1eaf45.tar.gz",
                "https://github.com/google/farmhash/archive/816a4ae622e964763ca0862d9dbd19324a1eaf45.tar.gz"
            ],
            "strip_prefix": "farmhash-816a4ae622e964763ca0862d9dbd19324a1eaf45",
            "build_file": "//third_party:farmhash.BUILD"
        },
        "repositories": [
            {
                "rule_class": "//third_party:repo.bzl%tf_http_archive",
                "attributes": {
                    "name": "farmhash_archive",
                    "generator_name": "farmhash_archive",
                    "generator_function": "tf_repositories",
                    "sha256": "6560547c63e4af82b0f202cb710ceabb3f21347a4b996db565a411da5b17aba0",
                    "urls": [
                        "https://storage.googleapis.com/mirror.tensorflow.org/github.com/google/farmhash/archive/816a4ae622e964763ca0862d9dbd19324a1eaf45.tar.gz",
                        "https://github.com/google/farmhash/archive/816a4ae622e964763ca0862d9dbd19324a1eaf45.tar.gz"
                    ],
                    "strip_prefix": "farmhash-816a4ae622e964763ca0862d9dbd19324a1eaf45",
                    "build_file": "//third_party:farmhash.BUILD"
                },
                "output_tree_hash": "ea39bdc8681e6856f6b36da537eb1a95d8288f3cff049819d14c9026bb6011ca"
            }
        ]
    },
    {
        "original_rule_class": "//third_party:repo.bzl%third_party_http_archive",
        "definition_information": "Repository kissfft instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule third_party_http_archive defined at:\n  /home/abuild/rpmbuild/BUILD/tensorflow-2.3.1/third_party/repo.bzl:219:43: in <toplevel>\n",
        "original_attributes": {
            "name": "kissfft",
            "generator_name": "kissfft",
            "generator_function": "tf_repositories",
            "sha256": "42b7ef406d5aa2d57a7b3b56fc44e8ad3011581692458a69958a911071efdcf2",
            "urls": [
                "https://storage.googleapis.com/mirror.tensorflow.org/github.com/mborgerding/kissfft/archive/36dbc057604f00aacfc0288ddad57e3b21cfc1b8.tar.gz",
                "https://github.com/mborgerding/kissfft/archive/36dbc057604f00aacfc0288ddad57e3b21cfc1b8.tar.gz"
            ],
            "strip_prefix": "kissfft-36dbc057604f00aacfc0288ddad57e3b21cfc1b8",
            "build_file": "//third_party/kissfft:BUILD.bazel"
        },
        "repositories": [
            {
                "rule_class": "//third_party:repo.bzl%third_party_http_archive",
                "attributes": {
                    "name": "kissfft",
                    "generator_name": "kissfft",
                    "generator_function": "tf_repositories",
                    "sha256": "42b7ef406d5aa2d57a7b3b56fc44e8ad3011581692458a69958a911071efdcf2",
                    "urls": [
                        "https://storage.googleapis.com/mirror.tensorflow.org/github.com/mborgerding/kissfft/archive/36dbc057604f00aacfc0288ddad57e3b21cfc1b8.tar.gz",
                        "https://github.com/mborgerding/kissfft/archive/36dbc057604f00aacfc0288ddad57e3b21cfc1b8.tar.gz"
                    ],
                    "strip_prefix": "kissfft-36dbc057604f00aacfc0288ddad57e3b21cfc1b8",
                    "build_file": "//third_party/kissfft:BUILD.bazel"
                },
                "output_tree_hash": "29d38278e4fc870a19a831aa0ed32d6384011e941dfcafffb9957502897b3c20"
            }
        ]
    },
    {
        "original_rule_class": "//third_party:repo.bzl%third_party_http_archive",
        "definition_information": "Repository cpuinfo instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule third_party_http_archive defined at:\n  /home/abuild/rpmbuild/BUILD/tensorflow-2.3.1/third_party/repo.bzl:219:43: in <toplevel>\n",
        "original_attributes": {
            "name": "cpuinfo",
            "generator_name": "cpuinfo",
            "generator_function": "tf_repositories",
            "sha256": "b1f2ee97e46d8917a66bcb47452fc510d511829556c93b83e06841b9b35261a5",
            "urls": [
                "https://storage.googleapis.com/mirror.tensorflow.org/github.com/pytorch/cpuinfo/archive/6cecd15784fcb6c5c0aa7311c6248879ce2cb8b2.zip",
                "https://github.com/pytorch/cpuinfo/archive/6cecd15784fcb6c5c0aa7311c6248879ce2cb8b2.zip"
            ],
            "strip_prefix": "cpuinfo-6cecd15784fcb6c5c0aa7311c6248879ce2cb8b2",
            "build_file": "//third_party/cpuinfo:BUILD.bazel",
            "patch_file": "//third_party/cpuinfo:cpuinfo.patch"
        },
        "repositories": [
            {
                "rule_class": "//third_party:repo.bzl%third_party_http_archive",
                "attributes": {
                    "name": "cpuinfo",
                    "generator_name": "cpuinfo",
                    "generator_function": "tf_repositories",
                    "sha256": "b1f2ee97e46d8917a66bcb47452fc510d511829556c93b83e06841b9b35261a5",
                    "urls": [
                        "https://storage.googleapis.com/mirror.tensorflow.org/github.com/pytorch/cpuinfo/archive/6cecd15784fcb6c5c0aa7311c6248879ce2cb8b2.zip",
                        "https://github.com/pytorch/cpuinfo/archive/6cecd15784fcb6c5c0aa7311c6248879ce2cb8b2.zip"
                    ],
                    "strip_prefix": "cpuinfo-6cecd15784fcb6c5c0aa7311c6248879ce2cb8b2",
                    "build_file": "//third_party/cpuinfo:BUILD.bazel",
                    "patch_file": "//third_party/cpuinfo:cpuinfo.patch"
                },
                "output_tree_hash": "9b264c054d7728c3dc259c175a4388e0d741f36c6592aa153e6e7b231670ebc8"
            }
        ]
    },
    {
        "original_rule_class": "//third_party:repo.bzl%tf_http_archive",
        "definition_information": "Repository mkl_dnn instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule tf_http_archive defined at:\n  /home/abuild/rpmbuild/BUILD/tensorflow-2.3.1/third_party/repo.bzl:134:34: in <toplevel>\n",
        "original_attributes": {
            "name": "mkl_dnn",
            "generator_name": "mkl_dnn",
            "generator_function": "tf_repositories",
            "sha256": "a0211aeb5e7dad50b97fa5dffc1a2fe2fe732572d4164e1ee8750a2ede43fbec",
            "urls": [
                "https://storage.googleapis.com/mirror.tensorflow.org/github.com/oneapi-src/oneDNN/archive/v0.21.3.tar.gz",
                "https://github.com/oneapi-src/oneDNN/archive/v0.21.3.tar.gz"
            ],
            "strip_prefix": "oneDNN-0.21.3",
            "build_file": "//third_party/mkl_dnn:mkldnn.BUILD"
        },
        "repositories": [
            {
                "rule_class": "//third_party:repo.bzl%tf_http_archive",
                "attributes": {
                    "name": "mkl_dnn",
                    "generator_name": "mkl_dnn",
                    "generator_function": "tf_repositories",
                    "sha256": "a0211aeb5e7dad50b97fa5dffc1a2fe2fe732572d4164e1ee8750a2ede43fbec",
                    "urls": [
                        "https://storage.googleapis.com/mirror.tensorflow.org/github.com/oneapi-src/oneDNN/archive/v0.21.3.tar.gz",
                        "https://github.com/oneapi-src/oneDNN/archive/v0.21.3.tar.gz"
                    ],
                    "strip_prefix": "oneDNN-0.21.3",
                    "build_file": "//third_party/mkl_dnn:mkldnn.BUILD"
                },
                "output_tree_hash": "bdc86bb700f7dc0c169d7bb6684fc8224471faec8a48e6894dbc57f63f7f8b1a"
            }
        ]
    },
    {
        "original_rule_class": "//third_party:repo.bzl%third_party_http_archive",
        "definition_information": "Repository pasta instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule third_party_http_archive defined at:\n  /home/abuild/rpmbuild/BUILD/tensorflow-2.3.1/third_party/repo.bzl:219:43: in <toplevel>\n",
        "original_attributes": {
            "name": "pasta",
            "generator_name": "pasta",
            "generator_function": "tf_repositories",
            "sha256": "c6dc1118250487d987a7b1a404425822def2e8fb2b765eeebc96887e982b6085",
            "urls": [
                "https://storage.googleapis.com/mirror.tensorflow.org/github.com/google/pasta/archive/v0.1.8.tar.gz",
                "https://github.com/google/pasta/archive/v0.1.8.tar.gz"
            ],
            "strip_prefix": "pasta-0.1.8",
            "build_file": "//third_party/pasta:BUILD.bazel",
            "system_build_file": "//third_party/pasta:BUILD.system"
        },
        "repositories": [
            {
                "rule_class": "//third_party:repo.bzl%third_party_http_archive",
                "attributes": {
                    "name": "pasta",
                    "generator_name": "pasta",
                    "generator_function": "tf_repositories",
                    "sha256": "c6dc1118250487d987a7b1a404425822def2e8fb2b765eeebc96887e982b6085",
                    "urls": [
                        "https://storage.googleapis.com/mirror.tensorflow.org/github.com/google/pasta/archive/v0.1.8.tar.gz",
                        "https://github.com/google/pasta/archive/v0.1.8.tar.gz"
                    ],
                    "strip_prefix": "pasta-0.1.8",
                    "build_file": "//third_party/pasta:BUILD.bazel",
                    "system_build_file": "//third_party/pasta:BUILD.system"
                },
                "output_tree_hash": "ed3ea82dcdd24a3b095b5bd9d9ac8680423831e0e371bb88d8ae63bb27f789cd"
            }
        ]
    },
    {
        "original_rule_class": "//third_party:repo.bzl%tf_http_archive",
        "definition_information": "Repository astunparse_archive instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule tf_http_archive defined at:\n  /home/abuild/rpmbuild/BUILD/tensorflow-2.3.1/third_party/repo.bzl:134:34: in <toplevel>\n",
        "original_attributes": {
            "name": "astunparse_archive",
            "generator_name": "astunparse_archive",
            "generator_function": "tf_repositories",
            "sha256": "5ad93a8456f0d084c3456d059fd9a92cce667963232cbf763eac3bc5b7940872",
            "urls": [
                "https://storage.googleapis.com/mirror.tensorflow.org/files.pythonhosted.org/packages/f3/af/4182184d3c338792894f34a62672919db7ca008c89abee9b564dd34d8029/astunparse-1.6.3.tar.gz",
                "https://files.pythonhosted.org/packages/f3/af/4182184d3c338792894f34a62672919db7ca008c89abee9b564dd34d8029/astunparse-1.6.3.tar.gz"
            ],
            "strip_prefix": "astunparse-1.6.3/lib",
            "build_file": "//third_party:astunparse.BUILD",
            "system_build_file": "//third_party/systemlibs:astunparse.BUILD"
        },
        "repositories": [
            {
                "rule_class": "//third_party:repo.bzl%tf_http_archive",
                "attributes": {
                    "name": "astunparse_archive",
                    "generator_name": "astunparse_archive",
                    "generator_function": "tf_repositories",
                    "sha256": "5ad93a8456f0d084c3456d059fd9a92cce667963232cbf763eac3bc5b7940872",
                    "urls": [
                        "https://storage.googleapis.com/mirror.tensorflow.org/files.pythonhosted.org/packages/f3/af/4182184d3c338792894f34a62672919db7ca008c89abee9b564dd34d8029/astunparse-1.6.3.tar.gz",
                        "https://files.pythonhosted.org/packages/f3/af/4182184d3c338792894f34a62672919db7ca008c89abee9b564dd34d8029/astunparse-1.6.3.tar.gz"
                    ],
                    "strip_prefix": "astunparse-1.6.3/lib",
                    "build_file": "//third_party:astunparse.BUILD",
                    "system_build_file": "//third_party/systemlibs:astunparse.BUILD"
                },
                "output_tree_hash": "78ee52386a4132b9884807595d036b1a69b8e0836745d4b87360e35e7193b7b8"
            }
        ]
    },
    {
        "original_rule_class": "//third_party:repo.bzl%tf_http_archive",
        "definition_information": "Repository fft2d instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule tf_http_archive defined at:\n  /home/abuild/rpmbuild/BUILD/tensorflow-2.3.1/third_party/repo.bzl:134:34: in <toplevel>\n",
        "original_attributes": {
            "name": "fft2d",
            "generator_name": "fft2d",
            "generator_function": "tf_repositories",
            "sha256": "5f4dabc2ae21e1f537425d58a49cdca1c49ea11db0d6271e2a4b27e9697548eb",
            "urls": [
                "https://storage.googleapis.com/mirror.tensorflow.org/github.com/petewarden/OouraFFT/archive/v1.0.tar.gz",
                "https://github.com/petewarden/OouraFFT/archive/v1.0.tar.gz"
            ],
            "strip_prefix": "OouraFFT-1.0",
            "build_file": "//third_party/fft2d:fft2d.BUILD"
        },
        "repositories": [
            {
                "rule_class": "//third_party:repo.bzl%tf_http_archive",
                "attributes": {
                    "name": "fft2d",
                    "generator_name": "fft2d",
                    "generator_function": "tf_repositories",
                    "sha256": "5f4dabc2ae21e1f537425d58a49cdca1c49ea11db0d6271e2a4b27e9697548eb",
                    "urls": [
                        "https://storage.googleapis.com/mirror.tensorflow.org/github.com/petewarden/OouraFFT/archive/v1.0.tar.gz",
                        "https://github.com/petewarden/OouraFFT/archive/v1.0.tar.gz"
                    ],
                    "strip_prefix": "OouraFFT-1.0",
                    "build_file": "//third_party/fft2d:fft2d.BUILD"
                },
                "output_tree_hash": "afd0a37a9a30d5b33886456e7136dab721b1e0bee5ecf3ef9b7b6ac9a5af05a5"
            }
        ]
    },
    {
        "original_rule_class": "//third_party:repo.bzl%tf_http_archive",
        "definition_information": "Repository org_sqlite instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule tf_http_archive defined at:\n  /home/abuild/rpmbuild/BUILD/tensorflow-2.3.1/third_party/repo.bzl:134:34: in <toplevel>\n",
        "original_attributes": {
            "name": "org_sqlite",
            "generator_name": "org_sqlite",
            "generator_function": "tf_repositories",
            "sha256": "b34f4c0c0eefad9a7e515c030c18702e477f4ef7d8ade6142bdab8011b487ac6",
            "urls": [
                "https://storage.googleapis.com/mirror.tensorflow.org/www.sqlite.org/2020/sqlite-amalgamation-3330000.zip",
                "https://www.sqlite.org/2020/sqlite-amalgamation-3330000.zip"
            ],
            "strip_prefix": "sqlite-amalgamation-3330000",
            "build_file": "//third_party:sqlite.BUILD",
            "system_build_file": "//third_party/systemlibs:sqlite.BUILD"
        },
        "repositories": [
            {
                "rule_class": "//third_party:repo.bzl%tf_http_archive",
                "attributes": {
                    "name": "org_sqlite",
                    "generator_name": "org_sqlite",
                    "generator_function": "tf_repositories",
                    "sha256": "b34f4c0c0eefad9a7e515c030c18702e477f4ef7d8ade6142bdab8011b487ac6",
                    "urls": [
                        "https://storage.googleapis.com/mirror.tensorflow.org/www.sqlite.org/2020/sqlite-amalgamation-3330000.zip",
                        "https://www.sqlite.org/2020/sqlite-amalgamation-3330000.zip"
                    ],
                    "strip_prefix": "sqlite-amalgamation-3330000",
                    "build_file": "//third_party:sqlite.BUILD",
                    "system_build_file": "//third_party/systemlibs:sqlite.BUILD"
                },
                "output_tree_hash": "648af5ed9d0a0fdfb3038120672fc9447a7ffd8159cbc78acd04a0ab16d3747a"
            }
        ]
    },
    {
        "original_rule_class": "//third_party:repo.bzl%tf_http_archive",
        "definition_information": "Repository boringssl instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule tf_http_archive defined at:\n  /home/abuild/rpmbuild/BUILD/tensorflow-2.3.1/third_party/repo.bzl:134:34: in <toplevel>\n",
        "original_attributes": {
            "name": "boringssl",
            "generator_name": "boringssl",
            "generator_function": "tf_repositories",
            "sha256": "a9c3b03657d507975a32732f04563132b4553c20747cec6dc04de475c8bdf29f",
            "urls": [
                "https://storage.googleapis.com/mirror.tensorflow.org/github.com/google/boringssl/archive/80ca9f9f6ece29ab132cce4cf807a9465a18cfac.tar.gz",
                "https://github.com/google/boringssl/archive/80ca9f9f6ece29ab132cce4cf807a9465a18cfac.tar.gz"
            ],
            "strip_prefix": "boringssl-80ca9f9f6ece29ab132cce4cf807a9465a18cfac",
            "system_build_file": "//third_party/systemlibs:boringssl.BUILD"
        },
        "repositories": [
            {
                "rule_class": "//third_party:repo.bzl%tf_http_archive",
                "attributes": {
                    "name": "boringssl",
                    "generator_name": "boringssl",
                    "generator_function": "tf_repositories",
                    "sha256": "a9c3b03657d507975a32732f04563132b4553c20747cec6dc04de475c8bdf29f",
                    "urls": [
                        "https://storage.googleapis.com/mirror.tensorflow.org/github.com/google/boringssl/archive/80ca9f9f6ece29ab132cce4cf807a9465a18cfac.tar.gz",
                        "https://github.com/google/boringssl/archive/80ca9f9f6ece29ab132cce4cf807a9465a18cfac.tar.gz"
                    ],
                    "strip_prefix": "boringssl-80ca9f9f6ece29ab132cce4cf807a9465a18cfac",
                    "system_build_file": "//third_party/systemlibs:boringssl.BUILD"
                },
                "output_tree_hash": "9a53145e08af05eefd9afb4655754bc2f789b4e98122304f5558726b57547ad3"
            }
        ]
    },
    {
        "original_rule_class": "@upb//bazel:repository_defs.bzl%bazel_version_repository",
        "definition_information": "Repository bazel_version instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule bazel_version_repository defined at:\n  /home/abuild/rpmbuild/BUILD/output_user_root/55287d6e880b1ebc99135e2e30fc9816/external/upb/bazel/repository_defs.bzl:12:43: in <toplevel>\n",
        "original_attributes": {
            "name": "bazel_version"
        },
        "repositories": [
            {
                "rule_class": "@upb//bazel:repository_defs.bzl%bazel_version_repository",
                "attributes": {
                    "name": "bazel_version"
                },
                "output_tree_hash": "6e13a6a1233be336770d55791c8cea0cc3fce9425e98200f3f1204379a6b4ae8"
            }
        ]
    },
    {
        "original_rule_class": "//third_party:repo.bzl%third_party_http_archive",
        "definition_information": "Repository icu instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule third_party_http_archive defined at:\n  /home/abuild/rpmbuild/BUILD/tensorflow-2.3.1/third_party/repo.bzl:219:43: in <toplevel>\n",
        "original_attributes": {
            "name": "icu",
            "generator_name": "icu",
            "generator_function": "tf_repositories",
            "sha256": "dfc62618aa4bd3ca14a3df548cd65fe393155edd213e49c39f3a30ccd618fc27",
            "urls": [
                "https://storage.googleapis.com/mirror.tensorflow.org/github.com/unicode-org/icu/archive/release-64-2.zip",
                "https://github.com/unicode-org/icu/archive/release-64-2.zip"
            ],
            "strip_prefix": "icu-release-64-2",
            "build_file": "//third_party/icu:BUILD.bazel",
            "system_build_file": "//third_party/icu:BUILD.system",
            "patch_file": "//third_party/icu:udata.patch"
        },
        "repositories": [
            {
                "rule_class": "//third_party:repo.bzl%third_party_http_archive",
                "attributes": {
                    "name": "icu",
                    "generator_name": "icu",
                    "generator_function": "tf_repositories",
                    "sha256": "dfc62618aa4bd3ca14a3df548cd65fe393155edd213e49c39f3a30ccd618fc27",
                    "urls": [
                        "https://storage.googleapis.com/mirror.tensorflow.org/github.com/unicode-org/icu/archive/release-64-2.zip",
                        "https://github.com/unicode-org/icu/archive/release-64-2.zip"
                    ],
                    "strip_prefix": "icu-release-64-2",
                    "build_file": "//third_party/icu:BUILD.bazel",
                    "system_build_file": "//third_party/icu:BUILD.system",
                    "patch_file": "//third_party/icu:udata.patch"
                },
                "output_tree_hash": "f589f3febd670626bce45b45e2f5f4823be3a6c084805a7341e89ee8cc2b46dc"
            }
        ]
    },
    {
        "original_rule_class": "//third_party:repo.bzl%third_party_http_archive",
        "definition_information": "Repository aws instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule third_party_http_archive defined at:\n  /home/abuild/rpmbuild/BUILD/tensorflow-2.3.1/third_party/repo.bzl:219:43: in <toplevel>\n",
        "original_attributes": {
            "name": "aws",
            "generator_name": "aws",
            "generator_function": "tf_repositories",
            "sha256": "758174f9788fed6cc1e266bcecb20bf738bd5ef1c3d646131c9ed15c2d6c5720",
            "urls": [
                "https://mirror.bazel.build/github.com/aws/aws-sdk-cpp/archive/1.7.336.tar.gz",
                "https://github.com/aws/aws-sdk-cpp/archive/1.7.336.tar.gz"
            ],
            "strip_prefix": "aws-sdk-cpp-1.7.336",
            "build_file": "//third_party/aws:BUILD.bazel"
        },
        "repositories": [
            {
                "rule_class": "//third_party:repo.bzl%third_party_http_archive",
                "attributes": {
                    "name": "aws",
                    "generator_name": "aws",
                    "generator_function": "tf_repositories",
                    "sha256": "758174f9788fed6cc1e266bcecb20bf738bd5ef1c3d646131c9ed15c2d6c5720",
                    "urls": [
                        "https://mirror.bazel.build/github.com/aws/aws-sdk-cpp/archive/1.7.336.tar.gz",
                        "https://github.com/aws/aws-sdk-cpp/archive/1.7.336.tar.gz"
                    ],
                    "strip_prefix": "aws-sdk-cpp-1.7.336",
                    "build_file": "//third_party/aws:BUILD.bazel"
                },
                "output_tree_hash": "fc65856b7ae473e0fdeeda046a78fa755fac18b43bc983c99926fa301b0112a7"
            }
        ]
    },
    {
        "original_rule_class": "//third_party:repo.bzl%third_party_http_archive",
        "definition_information": "Repository aws-c-event-stream instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule third_party_http_archive defined at:\n  /home/abuild/rpmbuild/BUILD/tensorflow-2.3.1/third_party/repo.bzl:219:43: in <toplevel>\n",
        "original_attributes": {
            "name": "aws-c-event-stream",
            "generator_name": "aws-c-event-stream",
            "generator_function": "tf_repositories",
            "sha256": "31d880d1c868d3f3df1e1f4b45e56ac73724a4dc3449d04d47fc0746f6f077b6",
            "urls": [
                "https://mirror.tensorflow.org/github.com/awslabs/aws-c-event-stream/archive/v0.1.4.tar.gz",
                "https://github.com/awslabs/aws-c-event-stream/archive/v0.1.4.tar.gz"
            ],
            "strip_prefix": "aws-c-event-stream-0.1.4",
            "build_file": "//third_party/aws:aws-c-event-stream.bazel"
        },
        "repositories": [
            {
                "rule_class": "//third_party:repo.bzl%third_party_http_archive",
                "attributes": {
                    "name": "aws-c-event-stream",
                    "generator_name": "aws-c-event-stream",
                    "generator_function": "tf_repositories",
                    "sha256": "31d880d1c868d3f3df1e1f4b45e56ac73724a4dc3449d04d47fc0746f6f077b6",
                    "urls": [
                        "https://mirror.tensorflow.org/github.com/awslabs/aws-c-event-stream/archive/v0.1.4.tar.gz",
                        "https://github.com/awslabs/aws-c-event-stream/archive/v0.1.4.tar.gz"
                    ],
                    "strip_prefix": "aws-c-event-stream-0.1.4",
                    "build_file": "//third_party/aws:aws-c-event-stream.bazel"
                },
                "output_tree_hash": "af0fb058f06d9c19eda5a9b46a177e503b70b8adedce59d191ed51b295808e3f"
            }
        ]
    },
    {
        "original_rule_class": "//third_party:repo.bzl%third_party_http_archive",
        "definition_information": "Repository aws-c-common instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule third_party_http_archive defined at:\n  /home/abuild/rpmbuild/BUILD/tensorflow-2.3.1/third_party/repo.bzl:219:43: in <toplevel>\n",
        "original_attributes": {
            "name": "aws-c-common",
            "generator_name": "aws-c-common",
            "generator_function": "tf_repositories",
            "sha256": "01c2a58553a37b3aa5914d9e0bf7bf14507ff4937bc5872a678892ca20fcae1f",
            "urls": [
                "https://mirror.tensorflow.org/github.com/awslabs/aws-c-common/archive/v0.4.29.tar.gz",
                "https://github.com/awslabs/aws-c-common/archive/v0.4.29.tar.gz"
            ],
            "strip_prefix": "aws-c-common-0.4.29",
            "build_file": "//third_party/aws:aws-c-common.bazel"
        },
        "repositories": [
            {
                "rule_class": "//third_party:repo.bzl%third_party_http_archive",
                "attributes": {
                    "name": "aws-c-common",
                    "generator_name": "aws-c-common",
                    "generator_function": "tf_repositories",
                    "sha256": "01c2a58553a37b3aa5914d9e0bf7bf14507ff4937bc5872a678892ca20fcae1f",
                    "urls": [
                        "https://mirror.tensorflow.org/github.com/awslabs/aws-c-common/archive/v0.4.29.tar.gz",
                        "https://github.com/awslabs/aws-c-common/archive/v0.4.29.tar.gz"
                    ],
                    "strip_prefix": "aws-c-common-0.4.29",
                    "build_file": "//third_party/aws:aws-c-common.bazel"
                },
                "output_tree_hash": "541fc8037d5ea89d4f207ce2a0b7201d6b8c38ebc3ba10e2ad28ca6463db4101"
            }
        ]
    },
    {
        "original_rule_class": "//third_party:repo.bzl%third_party_http_archive",
        "definition_information": "Repository aws-checksums instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule third_party_http_archive defined at:\n  /home/abuild/rpmbuild/BUILD/tensorflow-2.3.1/third_party/repo.bzl:219:43: in <toplevel>\n",
        "original_attributes": {
            "name": "aws-checksums",
            "generator_name": "aws-checksums",
            "generator_function": "tf_repositories",
            "sha256": "6e6bed6f75cf54006b6bafb01b3b96df19605572131a2260fddaf0e87949ced0",
            "urls": [
                "https://mirror.tensorflow.org/github.com/awslabs/aws-checksums/archive/v0.1.5.tar.gz",
                "https://github.com/awslabs/aws-checksums/archive/v0.1.5.tar.gz"
            ],
            "strip_prefix": "aws-checksums-0.1.5",
            "build_file": "//third_party/aws:aws-checksums.bazel"
        },
        "repositories": [
            {
                "rule_class": "//third_party:repo.bzl%third_party_http_archive",
                "attributes": {
                    "name": "aws-checksums",
                    "generator_name": "aws-checksums",
                    "generator_function": "tf_repositories",
                    "sha256": "6e6bed6f75cf54006b6bafb01b3b96df19605572131a2260fddaf0e87949ced0",
                    "urls": [
                        "https://mirror.tensorflow.org/github.com/awslabs/aws-checksums/archive/v0.1.5.tar.gz",
                        "https://github.com/awslabs/aws-checksums/archive/v0.1.5.tar.gz"
                    ],
                    "strip_prefix": "aws-checksums-0.1.5",
                    "build_file": "//third_party/aws:aws-checksums.bazel"
                },
                "output_tree_hash": "0a10cda49214bbc62aba456b546b3bed8903bdc07ce07b414320023d11c1a456"
            }
        ]
    },
    {
        "original_rule_class": "//third_party:repo.bzl%tf_http_archive",
        "definition_information": "Repository llvm-project instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule tf_http_archive defined at:\n  /home/abuild/rpmbuild/BUILD/tensorflow-2.3.1/third_party/repo.bzl:134:34: in <toplevel>\n",
        "original_attributes": {
            "name": "llvm-project",
            "generator_name": "llvm-project",
            "generator_function": "tf_repositories",
            "sha256": "a21b752ee1866e195f3f72c7931c79f8c4ecc0f14861488284bdc2bdf14d6fe9",
            "urls": [
                "https://storage.googleapis.com/mirror.tensorflow.org/github.com/llvm/llvm-project/archive/7e825abd5704ce28b166f9463d4bd304348fd2a9.tar.gz",
                "https://github.com/llvm/llvm-project/archive/7e825abd5704ce28b166f9463d4bd304348fd2a9.tar.gz"
            ],
            "strip_prefix": "llvm-project-7e825abd5704ce28b166f9463d4bd304348fd2a9",
            "additional_build_files": {
                "//third_party/llvm:llvm.autogenerated.BUILD": "llvm/BUILD",
                "//third_party/mlir:BUILD": "mlir/BUILD",
                "//third_party/mlir:test.BUILD": "mlir/test/BUILD"
            }
        },
        "repositories": [
            {
                "rule_class": "//third_party:repo.bzl%tf_http_archive",
                "attributes": {
                    "name": "llvm-project",
                    "generator_name": "llvm-project",
                    "generator_function": "tf_repositories",
                    "sha256": "a21b752ee1866e195f3f72c7931c79f8c4ecc0f14861488284bdc2bdf14d6fe9",
                    "urls": [
                        "https://storage.googleapis.com/mirror.tensorflow.org/github.com/llvm/llvm-project/archive/7e825abd5704ce28b166f9463d4bd304348fd2a9.tar.gz",
                        "https://github.com/llvm/llvm-project/archive/7e825abd5704ce28b166f9463d4bd304348fd2a9.tar.gz"
                    ],
                    "strip_prefix": "llvm-project-7e825abd5704ce28b166f9463d4bd304348fd2a9",
                    "additional_build_files": {
                        "//third_party/llvm:llvm.autogenerated.BUILD": "llvm/BUILD",
                        "//third_party/mlir:BUILD": "mlir/BUILD",
                        "//third_party/mlir:test.BUILD": "mlir/test/BUILD"
                    }
                },
                "output_tree_hash": "e78e179a0527a3ff09f57ccc7778e3a9344b8037b3a3a521b604e42a77bbd6fa"
            }
        ]
    },
    {
        "original_rule_class": "@io_bazel_rules_closure//closure:filegroup_external.bzl%filegroup_external",
        "definition_information": "Repository astunparse_license instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule filegroup_external defined at:\n  /home/abuild/rpmbuild/BUILD/output_user_root/55287d6e880b1ebc99135e2e30fc9816/external/io_bazel_rules_closure/closure/filegroup_external.bzl:186:37: in <toplevel>\n",
        "original_attributes": {
            "name": "astunparse_license",
            "generator_name": "astunparse_license",
            "generator_function": "tf_repositories",
            "sha256_urls": {
                "92fc0e4f4fa9460558eedf3412b988d433a2dcbb3a9c45402a145a4fab8a6ac6": [
                    "https://storage.googleapis.com/mirror.tensorflow.org/raw.githubusercontent.com/simonpercivall/astunparse/v1.6.2/LICENSE",
                    "https://raw.githubusercontent.com/simonpercivall/astunparse/v1.6.2/LICENSE"
                ]
            },
            "licenses": [
                "notice"
            ]
        },
        "repositories": [
            {
                "rule_class": "@io_bazel_rules_closure//closure:filegroup_external.bzl%filegroup_external",
                "attributes": {
                    "name": "astunparse_license",
                    "generator_name": "astunparse_license",
                    "generator_function": "tf_repositories",
                    "sha256_urls": {
                        "92fc0e4f4fa9460558eedf3412b988d433a2dcbb3a9c45402a145a4fab8a6ac6": [
                            "https://storage.googleapis.com/mirror.tensorflow.org/raw.githubusercontent.com/simonpercivall/astunparse/v1.6.2/LICENSE",
                            "https://raw.githubusercontent.com/simonpercivall/astunparse/v1.6.2/LICENSE"
                        ]
                    },
                    "licenses": [
                        "notice"
                    ]
                },
                "output_tree_hash": "5ab4b700d74876270769ebf42b8467ac09083d9eb05e880ddb7361bda78f7f11"
            }
        ]
    },
    {
        "original_rule_class": "@io_bazel_rules_closure//closure:filegroup_external.bzl%filegroup_external",
        "definition_information": "Repository org_python_license instantiated at:\n  no stack (--record_rule_instantiation_callstack not enabled)\nRepository rule filegroup_external defined at:\n  /home/abuild/rpmbuild/BUILD/output_user_root/55287d6e880b1ebc99135e2e30fc9816/external/io_bazel_rules_closure/closure/filegroup_external.bzl:186:37: in <toplevel>\n",
        "original_attributes": {
            "name": "org_python_license",
            "generator_name": "org_python_license",
            "generator_function": "tf_repositories",
            "sha256_urls": {
                "e76cacdf0bdd265ff074ccca03671c33126f597f39d0ed97bc3e5673d9170cf6": [
                    "https://storage.googleapis.com/mirror.tensorflow.org/docs.python.org/2.7/_sources/license.rst.txt",
                    "https://docs.python.org/2.7/_sources/license.rst.txt"
                ]
            },
            "licenses": [
                "notice"
            ]
        },
        "repositories": [
            {
                "rule_class": "@io_bazel_rules_closure//closure:filegroup_external.bzl%filegroup_external",
                "attributes": {
                    "name": "org_python_license",
                    "generator_name": "org_python_license",
                    "generator_function": "tf_repositories",
                    "sha256_urls": {
                        "e76cacdf0bdd265ff074ccca03671c33126f597f39d0ed97bc3e5673d9170cf6": [
                            "https://storage.googleapis.com/mirror.tensorflow.org/docs.python.org/2.7/_sources/license.rst.txt",
                            "https://docs.python.org/2.7/_sources/license.rst.txt"
                        ]
                    },
                    "licenses": [
                        "notice"
                    ]
                },
                "output_tree_hash": "5939eb8b5f575b1c2026f6f842dfe79076e6b8a9edd54898c4327e2140ed18fb"
            }
        ]
    }
]