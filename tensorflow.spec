%global _empty_manifest_terminate_build 0
Name:		tensorflow
Version:	2.3.1
Release:	5
Summary:	An Open Source Machine Learning Framework for Everyone
License:	Apache License 2.0
URL:		https://www.tensorflow.org/
Source0:	https://github.com/tensorflow/tensorflow/archive/v%{version}.tar.gz#/tensorflow-%{version}.tar.gz
#sh -x  updateSource1.sh
Source1:        external-%{_arch}.tar.bz2

Patch0001:      0001-Add-arm-source-file-into-aws-checksums.patch	
Patch0002:      CVE-2021-29538.patch
Patch0003:      CVE-2021-29535.patch
Patch0004:      CVE-2021-29566.patch
Patch0005:	CVE-2021-29534.patch
Patch0006:	fix_compile.patch
Requires:	python3-future
Requires:	python3-numpy

%description
TensorFlow is an end-to-end open source platform for machine learning. It has a comprehensive, flexible ecosystem of tools, libraries, and community resources that lets researchers push the state-of-the-art in ML and developers easily build and deploy ML-powered applications.

TensorFlow was originally developed by researchers and engineers working on the Google Brain team within Google's Machine Intelligence Research organization to conduct machine learning and deep neural networks research. The system is general enough to be applicable in a wide variety of other domains, as well.

TensorFlow provides stable Python and C++ APIs, as well as non-guaranteed backward compatible API for other languages.

%package -n python3-tensorflow
Summary:	An Open Source Machine Learning Framework for Everyone
Provides:	python-tensorflow
#It is because external-$(arch).tar.bz2 is built on bazel 3.5.0
BuildRequires:	bazel == 3.5.0 gcc gcc-c++
BuildRequires:	python3-numpy python3-devel python3-Keras-Preprocessing
BuildRequires:	python3-pip python3-wheel

%description -n python3-tensorflow
TensorFlow is an end-to-end open source platform for machine learning. It has a comprehensive, flexible ecosystem of tools, libraries, and community resources that lets researchers push the state-of-the-art in ML and developers easily build and deploy ML-powered applications.

TensorFlow was originally developed by researchers and engineers working on the Google Brain team within Google's Machine Intelligence Research organization to conduct machine learning and deep neural networks research. The system is general enough to be applicable in a wide variety of other domains, as well.

TensorFlow provides stable Python and C++ APIs, as well as non-guaranteed backward compatible API for other languages.

%prep
%autosetup -p1
extdir=$(bazel --output_user_root=`pwd`/../output_user_root info output_base)
mkdir -p ${extdir}
tar xf %{SOURCE1} -C ${extdir}
ln -sfn $(find ${extdir}/../install -name embedded_tools) ${extdir}/external/bazel_tools
ln -sfn $(find ${extdir}/../install -maxdepth 2 -name platforms) ${extdir}/external/platforms
rm -rf ${extdir}/external/local_config*

%build
bazel --output_user_root=`pwd`/../output_user_root build --host_copt=-Wno-stringop-truncation //tensorflow/tools/pip_package:build_pip_package
./bazel-bin/tensorflow/tools/pip_package/build_pip_package dist

%install
%py3_install_wheel *

%files -n python3-tensorflow
%{python3_sitearch}/*
%{_bindir}/*

%changelog
* Fri Aug 06 2021 linjiaxin5 <linjiaxin5@huawei.com> - 2.3.1-5
- Fix failure caused by GCC upgrade to 10
- add patch fix_compile.patch

* Mon Jun 28 2021 polite2anyone <zhangyao05@outlook,com> - 2.3.1-4
- Add patch CVE-2021-29534

* Fri Jun 25 2021 yaozc7 <yaozc7@foxmail.com> - 2.3.1-3
- Add patch CVE-2021-29538,CVE-2021-29535,CVE-2021-29566

* Mon May 31 2021 huanghaitao <huanghaitao8@huawei.com> - 2.3.1-2
- Completing build dependencies to fix gcc/gcc-c++ compiler missing error

* Wed Sep 30 2020 Zhipeng Xie<xiezhipeng1@huawei.com> - 2.3.1-1
- Package init
