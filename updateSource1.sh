#!/bin/bash
set -e
version=2.3.1
topdir=$(pwd)
builddir=/home/abuild/rpmbuild/BUILD
rm -rf ${builddir}
mkdir -p ${builddir}
cp tensorflow-${version}.tar.gz ${builddir}
cd ${builddir}
rm -rf tensorflow-${version}
rm -rf output_user_root
tar xf tensorflow-${version}.tar.gz
cd tensorflow-${version}
patch -p1 < ${topdir}/0001-Add-arm-source-file-into-aws-checksums.patch
#some packages will be checked in build
bazel --output_user_root=`pwd`/../output_user_root build //tensorflow/tools/pip_package:build_pip_package --experimental_repository_resolved_file=${topdir}/resolved-$(arch).bzl
bazel --output_user_root=`pwd`/../output_user_root clean
extdir=$(bazel --output_user_root=`pwd`/../output_user_root info output_base)
cd ${extdir}
rm -rf ${extdir}/external/local_config*
tar -cjf external-$(arch).tar.bz2 external
mv external-$(arch).tar.bz2 ${topdir}
cd ${topdir}
